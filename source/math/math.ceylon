import java.lang {
    JMath = Math
}

shared object math {
    native shared Float cos(Float x);
    native shared Float sin(Float x);
    native shared Float tan(Float x);
    native shared Float power(Float base, Float power);
    shared Integer abs(Integer x) => if(x < 0) then -x else x;
    shared Float pi = 3.14159265358979323846;

    native("jvm") shared Float cos(Float x) => JMath.cos(x);

    native("js") shared Float cos(Float x) {
        dynamic {
            return Math.cos(x);
        }
    }

    native("jvm") shared Float sin(Float x) => JMath.sin(x);

    native("js") shared Float sin(Float x) {
        dynamic {
            return Math.sin(x);
        }
    }

    native("jvm") shared Float tan(Float x) => JMath.tan(x);

    native("js") shared Float tan(Float x) {
        dynamic {
            return Math.tan(x);
        }
    }

    native("jvm") shared Float power(Float base, Float power) => JMath.pow(base, power);

    native("js") shared Float power(Float base, Float power) {
        dynamic {
            return Math.pow(base, power);
        }
    }

    shared Type min<Type>(Type first, Type second) given Type satisfies Comparable<Type> {
        if(first > second) {
            return second;
        } else {
            return first;
        }
    }

    shared Type max<Type>(Type first, Type second) given Type satisfies Comparable<Type> {
        if(first > second) {
            return first;
        } else {
            return second;
        }
    }

}