import com.jogamp.opengl{
    GLProfile,
    GLCapabilities,
    GLEventListener,
    GLAutoDrawable,
    TraceGL2,
    GL2
}
import com.jogamp.newt {...}
import com.jogamp.opengl.awt {...}
import javax.swing{JFrame}
import java.awt.event {
    WindowAdapter,
    WindowEvent,
    KeyListener,
    KeyEvent,
    MouseEvent,
    MouseAdapter
}
import java.lang{System{stdOut = \iout}}
import functionalRenderer.rendering {
    FunctionalRenderer
}
import math { math }
import joglRenderer { ... }
import functionalRenderer.model {
    Matrix4,
    Vector3
}
import com.jogamp.opengl.util {
    FPSAnimator,
    Animator
}
import demoCommon {
    Scene,
    Scene03,
    Scene02,
    Scene01,
    Scene04,
    Scene05,
    Scene06
}


shared void run() {
    GLProfile glprofile = GLProfile.getDefault(null);
    GLCapabilities glcapabilities = GLCapabilities( glprofile );
    GLJPanel gljpanel = GLJPanel( glcapabilities );

    variable Vector3<Float> position = Vector3(0.0, 0.0, 4.0);
    variable Float pitch = 0.0;
    variable Float yaw = 0.0;

    variable FunctionalRenderer? funcRenderer = null;
    variable Scene|Scene(FunctionalRenderer) scene = Scene05();
    variable Integer startTime = -1;

    variable Integer frames = 0;
    variable Integer measurementStartTime = -1;

    object listener satisfies GLEventListener {
        shared actual void display(GLAutoDrawable? drawable) {
//            print("Position: " + position.string + ", pitch: " + pitch.string + ", yaw: " + yaw.string);
            FunctionalRenderer? renderer = funcRenderer;
            if(exists renderer) {
                if(startTime == -1) {
                    startTime = system.milliseconds;
                }
                Scene|Scene(FunctionalRenderer) selectedScene = scene;
                Scene currentScene;
                if(is Scene selectedScene) {
                    currentScene = selectedScene;
                } else {
                    currentScene = selectedScene(renderer);
                    scene = currentScene;
                }

                Matrix4 view = Matrix4.view(position, pitch, yaw);
                renderer.render(currentScene.getScene(system.milliseconds - startTime, view));
            }
        }

        shared actual void dispose(GLAutoDrawable? drawable) {}

        shared actual void init(GLAutoDrawable? drawable) {
            FPSAnimator(drawable, 60).start();
            if(exists drawable) {
                variable GL2 gl = drawable.gl.gl2;
//                gl = TraceGL2(gl, stdOut);
                FunctionalRenderer renderer = FunctionalRenderer(JoglRenderer(gl));
                funcRenderer = renderer;
                scene = Scene01();
            }
        }

        shared actual void reshape(GLAutoDrawable? drawable, Integer x, Integer y, Integer width, Integer height) {
        }
    }

    gljpanel.addGLEventListener(listener);

    JFrame jframe = JFrame("Functional renderer");

    object winAdapter extends WindowAdapter() {
        suppressWarnings("expressionTypeNothing")
        shared actual void windowClosing(WindowEvent e) {
            jframe.dispose();
            print("Going to call process.exit(0)");
            process.exit(0);
        }
    }
    jframe.addWindowListener(winAdapter);
    jframe.focusable = true;
    jframe.addKeyListener(object satisfies KeyListener {
        shared actual void keyPressed(KeyEvent? e) {
            Float speed = 0.2;

            if(exists e) {
                // movement
                if(e.keyCode == KeyEvent.\iVK_RIGHT || e.keyCode == KeyEvent.\iVK_D) {
                    position = position + Vector3(math.cos(yaw) * speed, 0.0, -math.sin(yaw) * speed);
                }
                if(e.keyCode == KeyEvent.\iVK_LEFT || e.keyCode == KeyEvent.\iVK_A) {
                    position = position - Vector3(math.cos(yaw) * speed, 0.0, -math.sin(yaw) * speed);
                }
                if(e.keyCode == KeyEvent.\iVK_UP || e.keyCode == KeyEvent.\iVK_W) {
                    position = position + Vector3(-math.sin(yaw) * math.cos(pitch) * speed, math.sin(pitch) * speed, -math.cos(yaw) * math.cos(pitch) * speed);
                }
                if(e.keyCode == KeyEvent.\iVK_DOWN || e.keyCode == KeyEvent.\iVK_S) {
                    position = position - Vector3(-math.sin(yaw) * math.cos(pitch) * speed, math.sin(pitch) * speed, -math.cos(yaw) * math.cos(pitch) * speed);
                }
                if(e.keyCode == KeyEvent.\iVK_SHIFT) {
                    position = position + Vector3(0.0, speed, 0.0);
                }
                if(e.keyCode == KeyEvent.\iVK_CONTROL) {
                    position = position - Vector3(0.0, speed, 0.0);
                }
                // scenes
                if(e.keyCode == KeyEvent.\iVK_F1) {
                    scene = Scene01();
                }
                if(e.keyCode == KeyEvent.\iVK_F2) {
                    scene = Scene02();
                }
                if(e.keyCode == KeyEvent.\iVK_F3) {
                    FunctionalRenderer? renderer = funcRenderer;
                    if(exists renderer) {
                        scene = Scene03;
                    }
                }
                if(e.keyCode == KeyEvent.\iVK_F4) {
                    scene = Scene04();
                }
                if(e.keyCode == KeyEvent.\iVK_F5) {
                    scene = Scene05();
                }
                if(e.keyCode == KeyEvent.\iVK_F6) {
                    scene = Scene06();
                }
            }
        }

        shared actual void keyReleased(KeyEvent? e) {

        }

        shared actual void keyTyped(KeyEvent? e) {

        }
    });
    MouseAdapter mouseAdapter = object extends MouseAdapter() {
        variable Integer lastX = -1;
        variable Integer lastY = -1;

        shared actual void mouseDragged(MouseEvent? e) {
            if (exists e, lastX != -1, lastY != -1) {
                Float sensitivity = 0.005;

                yaw = yaw + (e.x - lastX) * sensitivity;
                pitch = pitch + (e.y - lastY) * sensitivity;
                pitch = math.max(pitch, -math.pi/2);
                pitch = math.min(pitch, math.pi/2);
                lastX = e.x;
                lastY = e.y;
            }
        }

        shared actual void mousePressed(MouseEvent? e) {
            if (exists e) {
                lastX = e.x;
                lastY = e.y;
            }
        }

        shared actual void mouseReleased(MouseEvent? e) {
            if(exists e) {
                lastX = -1;
                lastY = -1;
            }
        }

    };
    jframe.addMouseMotionListener(mouseAdapter);
    jframe.addMouseListener(mouseAdapter);

    jframe.contentPane.add(gljpanel);
    jframe.setSize(640, 640);
    jframe.setVisible(true);
}
