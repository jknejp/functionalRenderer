native("jvm") module demo "1.0.0" {
    import java.desktop "8";
    import java.base "8";
    import maven:"org.jogamp.gluegen:gluegen-rt" "2.3.1";
    import maven:"org.jogamp.jogl:jogl-all" "2.3.1";
    import functionalRenderer "1.0.0";
    import joglRenderer "1.0.0";
    import math "1.0.0";
    import demoCommon "1.0.0";

    import ceylon.http.server "1.3.2";
}