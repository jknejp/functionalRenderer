package demo;

import com.jogamp.opengl.GL2;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static com.jogamp.common.nio.Buffers.newDirectFloatBuffer;
import static com.jogamp.common.nio.Buffers.newDirectIntBuffer;

public class JavaRenderer {
    private static String[] vertexShader = null;
    private static String[] fragmentShader = null;

    private static int[] indexBuffer = null;
    private static float[] vertexBuffer = null;

    private static float[] projection = {
            1.74f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.74f, 0.0f, 0.0f,
            0.0f, 0.0f, -1.0f, -1.0f,
            0.0f, 0.0f, -0.2f, 0.0f,
    };

    private static float[] view = {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, -0.0f, 0.0f,
            -0.0f, 0.0f, 1.0f, 0.0f,
            -0.0f, -0.0f, -4.0f, 1.0f
    };
    private static boolean firstRender = true;
    private static int programId = 0;
    private static int teapotVertexBufferId = 0;
    private static int teapotIndexBufferId = 0;
    private static int bunnyVertexBufferId = 0;
    private static int bunnyIndexBufferId = 0;

    public static void render(GL2 gl, long time, boolean changeGeometry) {
        if(firstRender) {
            gl.setSwapInterval(0);
            vertexShader = readShaderProgram("resources/shaderPrograms/light_vert.txt");
            fragmentShader = readShaderProgram("resources/shaderPrograms/light_frag.txt");
            programId = createShader(gl, vertexShader, fragmentShader);
            loadTeapot();
            teapotIndexBufferId = loadIndexBuffer(gl, indexBuffer);
            teapotVertexBufferId = loadVertexBuffer(gl, vertexBuffer);
            loadBunny();
            bunnyIndexBufferId = loadIndexBuffer(gl, indexBuffer);
            bunnyVertexBufferId = loadVertexBuffer(gl, vertexBuffer);
            gl.glBindAttribLocation(programId, 0, "aVertexPosition");
            gl.glBindAttribLocation(programId, 1, "aNormal");
            gl.glLinkProgram(programId);
            firstRender = false;
        }
        gl.glViewport(0, 0, 640, 640);
        gl.glClearColor(0, 0, 0, 1);
        gl.glClear(gl.GL_COLOR_BUFFER_BIT|gl.GL_DEPTH_BUFFER_BIT);
        if(changeGeometry && time % 2 == 0) {
            gl.glBindBuffer(gl.GL_ARRAY_BUFFER, bunnyVertexBufferId);
            gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, bunnyIndexBufferId);
        } else {
            gl.glBindBuffer(gl.GL_ARRAY_BUFFER, teapotVertexBufferId);
            gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, teapotIndexBufferId);
        }
        gl.glEnable(gl.GL_DEPTH_TEST);
        gl.glDepthFunc(gl.GL_LEQUAL);
        gl.glUseProgram(programId );

        gl.glEnableVertexAttribArray(0);
        gl.glVertexAttribPointer(0, 3, gl.GL_FLOAT, false, 6 * 4, 0);
        gl.glEnableVertexAttribArray(1);
        gl.glVertexAttribPointer(1, 3, gl.GL_FLOAT, false, 6 * 4, 3 * 4);

        Matrix4 model = Matrix4.identity.rotateY(time/1000.0f);
        drawObject(gl, model);

        model = Matrix4.identity.translateY(-0.5f).scale(0.3f).rotateY(-time/300.0f).translateY(0.5f).translateZ(1.0f).rotateY(time/1000.0f);
        drawObject(gl, model);

        model = Matrix4.identity.translateY(-0.5f).scale(0.3f).rotateY(-time/300.0f).translateY(0.5f).translateZ(1.0f).rotateY(time/1000.0f).rotateY((float)Math.PI/2);
        drawObject(gl, model);

        model = Matrix4.identity.translateY(-0.5f).scale(0.3f).rotateY(-time/300.0f).translateY(0.5f).translateZ(1.0f).rotateY(time/1000.0f).rotateY((float)Math.PI);
        drawObject(gl, model);

        model = Matrix4.identity.translateY(-0.5f).scale(0.3f).rotateY(-time/300.0f).translateY(0.5f).translateZ(1.0f).rotateY(time/1000.0f).rotateY((float)-Math.PI/2);
        drawObject(gl, model);
    }

    private static void drawObject(GL2 gl, Matrix4 model) {
        setUniformMatrix(gl, programId, "view", view);
        setUniformMatrix(gl, programId, "model", model.getData());
        setUniformMatrix(gl, programId, "proj", projection);
        setUniformMatrix(gl, programId, "normal", model.inverse().transpose().getData());

        gl.glDrawElements(gl.GL_TRIANGLES, indexBuffer.length, gl.GL_UNSIGNED_INT, 0);
    }

    private static int createShader(GL2 gl, String[] vertexSrc, String[] fragmentSrc) {
        int vShader = gl.glCreateShader(gl.GL_VERTEX_SHADER);
        int[] vertexSrcLengths = new int[vertexSrc.length];
        for(int i = 0; i < vertexSrc.length; ++i) {
            vertexSrcLengths[i] = vertexSrc[i].length();
        }
        gl.glShaderSource(vShader, vertexSrc.length, vertexSrc, vertexSrcLengths, 0);
        gl.glCompileShader(vShader);
        printShaderError(gl, vShader);

        int fShader = gl.glCreateShader(gl.GL_FRAGMENT_SHADER);
        int[] fragmentSrcLengths = new int[fragmentSrc.length];
        for(int i = 0; i < fragmentSrc.length; ++i) {
            fragmentSrcLengths[i] = fragmentSrc[i].length();
        }
        gl.glShaderSource(fShader, fragmentSrc.length, fragmentSrc, fragmentSrcLengths, 0);
        gl.glCompileShader(fShader);
        printShaderError(gl, fShader);

        int programId = gl.glCreateProgram();
        gl.glAttachShader(programId, vShader);
        gl.glAttachShader(programId, fShader);

        return programId;
    }

    private static int loadIndexBuffer(GL2 gl, int[] indices) {
        int[] bufferId = new int[1];
        gl.glGenBuffers(1, bufferId, 0);
        gl.glBindBuffer(gl.GL_ELEMENT_ARRAY_BUFFER, bufferId[0]);
        gl.glBufferData(gl.GL_ELEMENT_ARRAY_BUFFER, indices.length * 4, newDirectIntBuffer(indices), gl.GL_STATIC_DRAW);
        return bufferId[0];
    }

    private static int loadVertexBuffer(GL2 gl, float[] vertices) {
        int[] bufferId = new int[1];
        gl.glGenBuffers(1, bufferId, 0);
        gl.glBindBuffer(gl.GL_ARRAY_BUFFER, bufferId[0]);
        gl.glBufferData(gl.GL_ARRAY_BUFFER, vertices.length * 4, newDirectFloatBuffer(vertices), gl.GL_STATIC_DRAW);
        return bufferId[0];
    }

    private static void setUniformMatrix(GL2 gl, int programId, String name, float[] value) {
        int location = gl.glGetUniformLocation(programId, name);
        gl.glUniformMatrix4fv(location, 1, true, newDirectFloatBuffer(value));
    }

    private static void loadTeapot() {
        loadObj("resources/objects/wt_teapot.obj");
    }

    private static void loadBunny() {
        loadObj("resources/objects/bunny.obj");
    }

    private static void loadObj(String path) {
        List<Float> vertices = new ArrayList<>();
        List<Float> normals = new ArrayList<>();
        List<Integer> indices = new ArrayList<>();
        List<Integer> normalIndices = new ArrayList<>();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
            String line = null;
            while((line = reader.readLine()) != null) {
                String[] split = line.split(" ");
                switch (split[0]) {
                    case "v":
                        for(int i = 1; i < 4; ++i) {
                            vertices.add(Float.parseFloat(split[i]));
                        }
                        break;
                    case "f":
                        for(int i = 1; i < 4; ++i) {
                            String[] innerSplit = split[i].split("/");
                            int first = Integer.parseInt(innerSplit[0]) - 1;
                            int last = Integer.parseInt(innerSplit[2]) - 1;
                            indices.add(first);
                            normalIndices.add(last);
                        }
                        break;
                    case "vn":
                        for(int i = 1; i < 4; ++i) {
                            normals.add(Float.parseFloat(split[i]));
                        }
                        break;
                }
            }

            int size = indices.size();
            vertexBuffer = new float[size * 6];
            indexBuffer = new int[size];
            for(int i = 0; i < size; ++i) {
                int ind = indices.get(i) * 3;
                int normInd = normalIndices.get(i) * 3;
                vertexBuffer[i * 6 + 0] = vertices.get(ind + 0);
                vertexBuffer[i * 6 + 1] = vertices.get(ind + 1);
                vertexBuffer[i * 6 + 2] = vertices.get(ind + 2);
                vertexBuffer[i * 6 + 3] = normals.get(normInd + 0);
                vertexBuffer[i * 6 + 4] = normals.get(normInd + 1);
                vertexBuffer[i * 6 + 5] = normals.get(normInd + 2);
                indexBuffer[i] = i;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String[] readShaderProgram(String path) {
        String[] shader = null;
        try {
            shader = Files.lines(Paths.get(path)).map(line -> line + "\n").toArray(String[]::new);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return shader;
    }

    private static void printShaderError(GL2 gl, int shader) {
        int[] logLength = new int[1];
        gl.glGetShaderiv(shader, gl.GL_INFO_LOG_LENGTH, logLength, 0);
        if(logLength[0] > 0) {
            byte[] errorMsg = new byte[logLength[0]];
            gl.glGetShaderInfoLog(shader, logLength[0], null, 0, errorMsg, 0);
            if(logLength[0] > 1) {
                System.err.println(new String(errorMsg));
            }
        }
    }

}
