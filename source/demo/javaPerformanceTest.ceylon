import javax.swing {
    JFrame
}
import com.jogamp.opengl.util {
    FPSAnimator,
    Animator
}
import joglRenderer {
    JoglRenderer
}
import com.jogamp.opengl.awt {
    GLJPanel
}
import  com.jogamp.opengl{
    GLProfile,
    GLCapabilities,
    GLEventListener,
    GLAutoDrawable,
    TraceGL2,
    GL2
}
import com.jogamp.newt {...}
import java.awt.event {
    WindowAdapter,
    WindowEvent,
    KeyListener,
    KeyEvent,
    MouseEvent,
    MouseAdapter
}
import java.lang{System{stdOut = \iout}}
import functionalRenderer.rendering {
    FunctionalRenderer
}
import math { math }
import functionalRenderer.model {
    Matrix4,
    Vector3
}
import demoCommon {
    Scene,
    Scene03,
    Scene02,
    Scene01,
    Scene04,
    Scene05,
    Scene06
}
import ceylon.collection {
    ArrayList
}
import ceylon.file {
    File,
    parsePath
}

shared void runJavaPerformanceTest() {
    GLProfile glprofile = GLProfile.getDefault(null);
    GLCapabilities glcapabilities = GLCapabilities( glprofile );
    GLJPanel gljpanel = GLJPanel( glcapabilities );

    variable Scene|Scene(FunctionalRenderer) scene = Scene05();
    variable Integer startTime = -1;

    variable Integer frames = 0;
    variable Integer measurementStartTime = -1;

    Integer testFrameCount = 10000;
    ArrayList<Integer> testValues = ArrayList<Integer>(testFrameCount);

    String cmdArg = process.namedArgumentValue("changeGeometry") else "false";
    object listener satisfies GLEventListener {
        shared actual void display(GLAutoDrawable? drawable) {
            if(startTime == -1) {
                startTime = system.milliseconds;
            }
            assert (exists drawable, exists gl2 = drawable.gl.gl2);
            JavaRenderer.render(gl2, startTime-system.milliseconds, cmdArg == "true");

            frames += 1;

            if(frames == testFrameCount) {
                value took = system.milliseconds - startTime;
                print(took);
                value fps = testFrameCount.float / took * 1000;
                print(fps.string + "fps");
            }
        }

        shared actual void dispose(GLAutoDrawable? drawable) {}

        shared actual void init(GLAutoDrawable? drawable) {
            value animator = Animator(drawable);
            animator.setRunAsFastAsPossible(true);
            animator.start();
        }

        shared actual void reshape(GLAutoDrawable? drawable, Integer x, Integer y, Integer width, Integer height) {
        }
    }

    gljpanel.addGLEventListener(listener);

    JFrame jframe = JFrame("Java test");

    object winAdapter extends WindowAdapter() {
        suppressWarnings("expressionTypeNothing")
        shared actual void windowClosing(WindowEvent e) {
            jframe.dispose();
            print("Going to call process.exit(0)");
            process.exit(0);
        }
    }
    jframe.addWindowListener(winAdapter);
    jframe.focusable = true;

    jframe.contentPane.add(gljpanel);
    jframe.setSize(640, 640);
    jframe.setVisible(true);
}

void writeJavaResults(ArrayList<Integer> testValues) {
    String path = "source/demo/javaResults.csv";
    value filePath = parsePath(path);
    if(is File file = filePath.resource) {
        value writer = file.Overwriter();

        for(time in testValues) {
            writer.writeLine(time.string);
        }
        print("Result written");
    } else {
        print("cannot find file");
    }
}