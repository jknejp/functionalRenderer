package demo;

public class Matrix4 {
    private static float[] identityData = {
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 1.0f
    };
    public static final Matrix4 identity = new Matrix4(identityData);

    float[] data;

    public Matrix4(float[] data) {
        this.data = data;
    }

    public float[] getData() {
        return data;
    }

    public Matrix4 translateX(float distance) {
        float[] translation = {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            distance, 0.0f, 0.0f, 1.0f
        };
        return this.mul(new Matrix4(translation));
    }

    public Matrix4 translateY(float distance) {
        float[] translation = {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, distance, 0.0f, 1.0f
        };
        return this.mul(new Matrix4(translation));
    }

    public Matrix4 translateZ(float distance) {
        float[] translation = {
            1.0f, 0.0f, 0.0f, 0.0f,
            0.0f, 1.0f, 0.0f, 0.0f,
            0.0f, 0.0f, 1.0f, 0.0f,
            0.0f, 0.0f, distance, 1.0f
        };
        return this.mul(new Matrix4(translation));
    }

    public Matrix4 rotateX(float angle) {
        float[] translation = {
                1.0f, 0.0f, 0.0f, 0.0f,
                0.0f, (float)Math.cos(angle), (float)-Math.sin(angle), 0.0f,
                0.0f, (float)Math.sin(angle), (float)Math.cos(angle), 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f
        };
        return this.mul(new Matrix4(translation));
    }

    public Matrix4 rotateY(float angle) {
        float[] translation = {
                (float)Math.cos(angle),0.0f, (float)Math.sin(angle), 0.0f,
                0.0f, 1.0f, 0.0f, 0.0f,
                (float)-Math.sin(angle), 0.0f, (float)Math.cos(angle), 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f
        };
        return this.mul(new Matrix4(translation));
    }

    public Matrix4 scale(float ratio) {
        float[] translation = {
                ratio, 0.0f, 0.0f, 0.0f,
                0.0f, ratio, 0.0f, 0.0f,
                0.0f, 0.0f, ratio, 0.0f,
                0.0f, 0.0f, 0.0f, 1.0f
        };
        return this.mul(new Matrix4(translation));
    }

    public Matrix4 mul(Matrix4 other) {
        float[] result = {
            data[0 * 4 + 0] * other.data[0 * 4 + 0] + data[0 * 4 + 1] * other.data[1 * 4 + 0] + data[0 * 4 + 2] * other.data[2 * 4 + 0] + data[0 * 4 + 3] * other.data[3 * 4 + 0],
            data[0 * 4 + 0] * other.data[0 * 4 + 1] + data[0 * 4 + 1] * other.data[1 * 4 + 1] + data[0 * 4 + 2] * other.data[2 * 4 + 1] + data[0 * 4 + 3] * other.data[3 * 4 + 1],
            data[0 * 4 + 0] * other.data[0 * 4 + 2] + data[0 * 4 + 1] * other.data[1 * 4 + 2] + data[0 * 4 + 2] * other.data[2 * 4 + 2] + data[0 * 4 + 3] * other.data[3 * 4 + 2],
            data[0 * 4 + 0] * other.data[0 * 4 + 3] + data[0 * 4 + 1] * other.data[1 * 4 + 3] + data[0 * 4 + 2] * other.data[2 * 4 + 3] + data[0 * 4 + 3] * other.data[3 * 4 + 3],
            data[1 * 4 + 0] * other.data[0 * 4 + 0] + data[1 * 4 + 1] * other.data[1 * 4 + 0] + data[1 * 4 + 2] * other.data[2 * 4 + 0] + data[1 * 4 + 3] * other.data[3 * 4 + 0],
            data[1 * 4 + 0] * other.data[0 * 4 + 1] + data[1 * 4 + 1] * other.data[1 * 4 + 1] + data[1 * 4 + 2] * other.data[2 * 4 + 1] + data[1 * 4 + 3] * other.data[3 * 4 + 1],
            data[1 * 4 + 0] * other.data[0 * 4 + 2] + data[1 * 4 + 1] * other.data[1 * 4 + 2] + data[1 * 4 + 2] * other.data[2 * 4 + 2] + data[1 * 4 + 3] * other.data[3 * 4 + 2],
            data[1 * 4 + 0] * other.data[0 * 4 + 3] + data[1 * 4 + 1] * other.data[1 * 4 + 3] + data[1 * 4 + 2] * other.data[2 * 4 + 3] + data[1 * 4 + 3] * other.data[3 * 4 + 3],
            data[2 * 4 + 0] * other.data[0 * 4 + 0] + data[2 * 4 + 1] * other.data[1 * 4 + 0] + data[2 * 4 + 2] * other.data[2 * 4 + 0] + data[2 * 4 + 3] * other.data[3 * 4 + 0],
            data[2 * 4 + 0] * other.data[0 * 4 + 1] + data[2 * 4 + 1] * other.data[1 * 4 + 1] + data[2 * 4 + 2] * other.data[2 * 4 + 1] + data[2 * 4 + 3] * other.data[3 * 4 + 1],
            data[2 * 4 + 0] * other.data[0 * 4 + 2] + data[2 * 4 + 1] * other.data[1 * 4 + 2] + data[2 * 4 + 2] * other.data[2 * 4 + 2] + data[2 * 4 + 3] * other.data[3 * 4 + 2],
            data[2 * 4 + 0] * other.data[0 * 4 + 3] + data[2 * 4 + 1] * other.data[1 * 4 + 3] + data[2 * 4 + 2] * other.data[2 * 4 + 3] + data[2 * 4 + 3] * other.data[3 * 4 + 3],
            data[3 * 4 + 0] * other.data[0 * 4 + 0] + data[3 * 4 + 1] * other.data[1 * 4 + 0] + data[3 * 4 + 2] * other.data[2 * 4 + 0] + data[3 * 4 + 3] * other.data[3 * 4 + 0],
            data[3 * 4 + 0] * other.data[0 * 4 + 1] + data[3 * 4 + 1] * other.data[1 * 4 + 1] + data[3 * 4 + 2] * other.data[2 * 4 + 1] + data[3 * 4 + 3] * other.data[3 * 4 + 1],
            data[3 * 4 + 0] * other.data[0 * 4 + 2] + data[3 * 4 + 1] * other.data[1 * 4 + 2] + data[3 * 4 + 2] * other.data[2 * 4 + 2] + data[3 * 4 + 3] * other.data[3 * 4 + 2],
            data[3 * 4 + 0] * other.data[0 * 4 + 3] + data[3 * 4 + 1] * other.data[1 * 4 + 3] + data[0 * 4 + 3] * other.data[2 * 4 + 3] + data[3 * 4 + 3] * other.data[3 * 4 + 3]
        };

        return new Matrix4(result);
    }

    public Matrix4 transpose() {
        float[] transposedData = {
            data[0 * 4 + 0],
            data[1 * 4 + 0],
            data[2 * 4 + 0],
            data[3 * 4 + 0],
            data[0 * 4 + 1],
            data[1 * 4 + 1],
            data[2 * 4 + 1],
            data[3 * 4 + 1],
            data[0 * 4 + 2],
            data[1 * 4 + 2],
            data[2 * 4 + 2],
            data[3 * 4 + 2],
            data[0 * 4 + 3],
            data[1 * 4 + 3],
            data[2 * 4 + 3],
            data[3 * 4 + 3]
        };

        return new Matrix4(transposedData);
    }

    public Matrix4 inverse() {
        float s0 = data[0 * 4 + 0] * data[1 * 4 + 1] - data[1 * 4 + 0] * data[0 * 4 + 1];
        float s1 = data[0 * 4 + 0] * data[1 * 4 + 2] - data[1 * 4 + 0] * data[0 * 4 + 2];
        float s2 = data[0 * 4 + 0] * data[1 * 4 + 3] - data[1 * 4 + 0] * data[0 * 4 + 3];
        float s3 = data[0 * 4 + 1] * data[1 * 4 + 2] - data[1 * 4 + 1] * data[0 * 4 + 2];
        float s4 = data[0 * 4 + 1] * data[1 * 4 + 3] - data[1 * 4 + 1] * data[0 * 4 + 3];
        float s5 = data[0 * 4 + 2] * data[1 * 4 + 3] - data[1 * 4 + 2] * data[0 * 4 + 3];
        float c5 = data[2 * 4 + 2] * data[3 * 4 + 3] - data[3 * 4 + 2] * data[2 * 4 + 3];
        float c4 = data[2 * 4 + 1] * data[3 * 4 + 3] - data[3 * 4 + 1] * data[2 * 4 + 3];
        float c3 = data[2 * 4 + 1] * data[3 * 4 + 2] - data[3 * 4 + 1] * data[2 * 4 + 2];
        float c2 = data[2 * 4 + 0] * data[3 * 4 + 3] - data[3 * 4 + 0] * data[2 * 4 + 3];
        float c1 = data[2 * 4 + 0] * data[3 * 4 + 2] - data[3 * 4 + 0] * data[2 * 4 + 2];
        float c0 = data[2 * 4 + 0] * data[3 * 4 + 1] - data[3 * 4 + 0] * data[2 * 4 + 1];

        float inverseDeterminant = 1 / (s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0);

        float[] resultData = {
            (data[1 * 4 +1] * c5 - data[1 * 4 + 2] * c4 + data[1 * 4 + 3] * c3) * inverseDeterminant,
            (-data[0 * 4 +1] * c5 + data[0 * 4 + 2] * c4 - data[0 * 4 + 3] * c3) * inverseDeterminant,
            (data[3 * 4 +1] * s5 - data[3 * 4 + 2] * s4 + data[3 * 4 + 3] * s3) * inverseDeterminant,
            (-data[2 * 4 +1] * s5 + data[2 * 4 + 2] * s4 - data[2 * 4 + 3] * s3) * inverseDeterminant,
            (-data[1 * 4 +0] * c5 + data[1 * 4 + 2] * c2 - data[1 * 4 + 3] * c1) * inverseDeterminant,
            (data[0 * 4 +0] * c5 - data[0 * 4 + 2] * c2 + data[0 * 4 + 3] * c1) * inverseDeterminant,
            (-data[3 * 4 +0] * s5 + data[3 * 4 + 2] * s2 - data[3 * 4 + 3] * s1) * inverseDeterminant,
            (data[2 * 4 +0] * s5 - data[2 * 4 + 2] * s2 + data[2 * 4 + 3] * s1) * inverseDeterminant,
            (data[1 * 4 +0] * c4 - data[1 * 4 + 1] * c2 + data[1 * 4 + 3] * c0) * inverseDeterminant,
            (-data[0 * 4 +0] * c4 + data[0 * 4 + 1] * c2 - data[0 * 4 + 3] * c0) * inverseDeterminant,
            (data[3 * 4 +0] * s4 - data[3 * 4 + 1] * s2 + data[3 * 4 + 3] * s0) * inverseDeterminant,
            (-data[2 * 4 +0] * s4 + data[2 * 4 + 1] * s2 - data[2 * 4 + 3] * s0) * inverseDeterminant,
            (-data[1 * 4 +0] * c3 + data[1 * 4 + 1] * c1 - data[1 * 4 + 2] * c0) * inverseDeterminant,
            (data[0 * 4 +0] * c3 - data[0 * 4 + 1] * c1 + data[0 * 4 + 2] * c0) * inverseDeterminant,
            (-data[3 * 4 +0] * s3 + data[3 * 4 + 1] * s1 - data[3 * 4 + 2] * s0) * inverseDeterminant,
            (data[2 * 4 +0] * s3 - data[2 * 4 + 1] * s1 + data[2 * 4 + 2] * s0) * inverseDeterminant
        };

        return new Matrix4(resultData);
    }
}
