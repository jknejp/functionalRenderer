import java.awt{Desktop{currentDesktop = \iDesktop, desktopSupported = \iDesktopSupported}}
import ceylon.http.server {
    newServer,
    Request,
    Response,
    TemplateMatcher,
    Server,
    Endpoint,
    Status,
    started,
    AsynchronousEndpoint,
    startsWith
}
import java.net {
    URI
}
import ceylon.http.server.endpoints {
    RepositoryEndpoint,
    serveStaticFile
}
import ceylon.http.common {
    get
}

Endpoint createSimpleEndpoint(String html, String path) {
    value service = (Request request, Response response) => response.writeString(html);
    Endpoint indexEp = Endpoint(TemplateMatcher("/" + path), service, {get} );
    return indexEp;
}

shared void runJs() {
    RepositoryEndpoint modulesEp = RepositoryEndpoint("/modules");

    value indexEp = createSimpleEndpoint(html, "index");
    value ceylonPerformaceEp = createSimpleEndpoint(htmlCeylonPerf, "ceylonPerformance");
    value javascriptPerformaceEp = createSimpleEndpoint(htmlJsPerf, "javascriptPerformance");
    function mapper(Request req)
            => req.path;
    value staticEp = AsynchronousEndpoint(
        startsWith("/"),
        serveStaticFile("resources", mapper),
        {get}
    );


    Server server = newServer({indexEp, ceylonPerformaceEp, javascriptPerformaceEp, modulesEp, staticEp});
    server.addListener(
        void (Status status) {
            switch (status)
            case (started) {
                openBrowser();
            }
            else {
            }
        }
    );
    server.start();
}

void openBrowser() {
    if(desktopSupported){
        Desktop desktop = currentDesktop;
        desktop.browse(URI("http://localhost:8080/index"));
    } else {
        print("Cannot open browser");
    }

}

String html =
"""
   <!DOCTYPE html>
   <html lang="en">
   <head>
   <meta charset="UTF-8">
   <title>Test</title>
   <script src="js/require.js"></script>
   </head>
   <body>
   <canvas id="canvas">

   </canvas>
   <div id="menu">

   </div>
   <script type="text/javascript">
    require.config({
        baseUrl : 'modules'
    });
    require(
            [ 'demoJs/1.0.0/demoJs-1.0.0' ],
            function(app) {
                app.run();
            }
    );
    </script>
   </body>
   </html>
""";

String htmlCeylonPerf =
"""
   <!DOCTYPE html>
   <html lang="en">
   <head>
   <meta charset="UTF-8">
   <title>Test</title>
   <script src="js/require.js"></script>
   </head>
   <body>
   <canvas id="canvas">

   </canvas>
   <div id="menu">

   </div>
   <script type="text/javascript">
    require.config({
        baseUrl : 'modules'
    });
    require(
            [ 'demoJs/1.0.0/demoJs-1.0.0' ],
            function(app) {
                app.runCeylonPerformance();
            }
    );
    </script>
   </body>
   </html>
""";

String htmlJsPerf =
"""
   <!DOCTYPE html>
   <html lang="en">
   <head>
   <meta charset="UTF-8">
   <title>Test</title>
   <script src="js/require.js"></script>
   <script src="js/Matrix.js"></script>
   <script src="js/JavascriptRenderer.js"></script>
   </head>
   <body>
   <canvas id="canvas">

   </canvas>
   <div id="menu">

   </div>
   <script type="text/javascript">
    require.config({
        baseUrl : 'modules'
    });
    require(
            [ 'demoJs/1.0.0/demoJs-1.0.0' ],
            function(app) {
                app.runJavascriptPerformance();
            }
    );
    </script>
   </body>
   </html>
""";