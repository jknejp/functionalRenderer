import functionalRenderer.model {
    SceneRoot,
    Matrix4,
    Uniform,
    Subscene,
    Colour,
    ShaderProgram,
    mat4Identity,
    Viewport
}
import math {
    math
}
import demoCommon.utils {
    loadShader
}

shared class Scene01() satisfies Scene {
    ShaderProgram sp = ShaderProgram(loadShader("simple_vert"), loadShader("simple_frag"));

    shared actual SceneRoot getScene(Integer time, Matrix4 view) {
        Matrix4 projection = Matrix4.perspective(0.1, 100.0, math.pi / 3.0);

        return SceneRoot {
            viewport = Viewport(0, 0, 640, 640);
            shaderProgram = sp;
            children = [
                Subscene {
                    geometry = sharedGeometry.cube;
                    uniformValues = [
                        Uniform {
                            name = "model";
                            uniformValue = mat4Identity.rotateY(0.5).rotateX(time.float/1000);
                        },
                        Uniform {
                            name = "view";
                            uniformValue = view;
                        },
                        Uniform {
                            name = "proj";
                            uniformValue = projection;
                        }
                    ];
                }
            ];
        };
    }
}