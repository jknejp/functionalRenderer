import functionalRenderer.model {
    SceneRoot,
    Matrix4,
    Uniform,
    Subscene,
    Colour,
    ShaderProgram,
    mat4Identity,
    RenderPass,
    Vector3,
    FramebufferSetting,
    Viewport
}
import math {
    math
}
import demoCommon.utils {
    loadShader,
    clampToEdgeAndLinear
}

shared class Scene05() satisfies Scene {
    ShaderProgram sp = ShaderProgram(loadShader("texCoor_vert"), loadShader("texCoor_frag"));
    Scene04 renderPassScene = Scene04();

    shared actual SceneRoot getScene(Integer time, Matrix4 view) {

        Matrix4 projection = Matrix4.perspective(0.1, 100.0, math.pi / 3.0);

        return SceneRoot {
            clearColour = Colour(0.0, 0.5, 0.5, 1.0);
            viewport = Viewport(0, 0, 640, 640);
            shaderProgram = sp;
            children = [
                Subscene {
                    geometry = sharedGeometry.cube2;
                    uniformValues = [
                        Uniform {
                            name = "model";
                            uniformValue = mat4Identity;
                        },
                        Uniform {
                            name = "view";
                            uniformValue = view;
                        },
                        Uniform {
                            name = "proj";
                            uniformValue = projection;
                        },
                        Uniform {
                            name = "uTex";
                            uniformValue = RenderPass{
                                scene = renderPassScene.getScene(time, Matrix4.view(Vector3(0.0, 0.0, 4.0), 0.0, 0.0));
                                framebufferSettings = FramebufferSetting(640, 640);
                                textureSettings = clampToEdgeAndLinear;
                            };
                        }
                    ];
                }
            ];
        };
    }
}