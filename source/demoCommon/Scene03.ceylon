import functionalRenderer.model {
    SceneRoot,
    Matrix4,
    Uniform,
    ShaderAttribute,
    Subscene,
    Colour,
    ShaderProgram,
    mat4Identity,
    Texture,
    Viewport,
    TextureUsage
}
import math {
    math
}
import functionalRenderer.rendering {
    FunctionalRenderer
}
import demoCommon.utils {
    loadShader,
    loadImage,
    clampToEdgeAndLinear
}

shared class Scene03(FunctionalRenderer renderer) satisfies Scene {
    Texture texture1 = renderer.getTexture(loadImage("brick.jpg"));
    Texture texture2 = renderer.getTexture(loadImage("rock.jpg"));
    ShaderProgram sp = ShaderProgram(loadShader("texCoor_vert"), loadShader("texCoor_frag"));

    shared actual SceneRoot getScene(Integer time, Matrix4 view) {
        Matrix4 projection = Matrix4.perspective(0.1, 100.0, math.pi / 3.0);

        return SceneRoot {
            viewport = Viewport(0, 0, 640, 640);
            shaderProgram = sp;
            children = [
                Subscene {
                    geometry = sharedGeometry.cube2;
                    uniformValues = [
                        Uniform {
                            name = "model";
                            uniformValue = mat4Identity;
                        },
                        Uniform {
                            name = "view";
                            uniformValue = view;
                        },
                        Uniform {
                            name = "proj";
                            uniformValue = projection;
                        }, 
                        Uniform {
                            name = "uTex";
                            uniformValue = getTexture(time);
                        }
                    ];
                }
            ];
        };
    }

    TextureUsage getTexture(Integer time) {
        if ((time % 10000)<5000) {
            return TextureUsage(texture2, clampToEdgeAndLinear);
        } else {
            return TextureUsage(texture1, clampToEdgeAndLinear);
        }
    }
}