"Default documentation for module `demoCommon`."

module demoCommon "1.0.0" {
    shared import functionalRenderer "1.0.0";
    import math "1.0.0";
    import ceylon.collection "1.3.2";
    native("jvm") import ceylon.file "1.3.2";
    native("js") import ceylon.interop.browser "1.3.2";
    import ceylon.regex "1.3.2";
    native("jvm") import joglRenderer "1.0.0";
    native("js") import webglRenderer "1.0.0";
}
