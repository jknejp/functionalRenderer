import functionalRenderer.model {
    SceneRoot,
    Matrix4,
    Uniform,
    ShaderAttribute,
    Subscene,
    Colour,
    ShaderProgram,
    mat4Identity,
    Viewport
}
import math {
    math
}
import option {
    Option
}
import demoCommon.utils {
    loadShader
}

shared class Scene02() satisfies Scene {
    ShaderProgram sp = ShaderProgram(loadShader("simple_vert"), loadShader("simple_frag"));

    shared actual SceneRoot getScene(Integer time, Matrix4 view) {
        Matrix4 projection = Matrix4.perspective(0.1, 100.0, math.pi / 3.0);

        Subscene smallCube = Subscene {
            geometry = sharedGeometry.cube;
            uniformValues = [
                Uniform<Matrix4>("model", (Option<Matrix4> parent) =>
                mat4Identity
                    .rotateZ(math.pi/4)
                    .rotateX(time/500.0)
                    .scale(0.3)
                    .translateZ(1.0 + math.power(3.0, 1.0/2.0) * 0.3)
                    .mul(parent.getOrDefault(mat4Identity))
                )
            ];
        };

        return SceneRoot {
            viewport = Viewport(0, 0, 640, 640);
            shaderProgram = sp;
            children = [
                Subscene {
                    uniformValues = [
                        Uniform {
                            name = "model";
                            uniformValue = mat4Identity.rotateY(time/4000.0);
                        },
                        Uniform {
                            name = "view";
                            uniformValue = view;
                        },
                        Uniform {
                            name = "proj";
                            uniformValue = projection;
                        }
                    ];
                    geometry = sharedGeometry.cube;
                    children = [
                        Subscene {
                            children = [
                                smallCube
                            ];
                        },
                        Subscene {
                            uniformValues = [
                                Uniform<Matrix4> {
                                    name = "model";
                                    uniformValue = (Option<Matrix4> mat) =>
                                        mat4Identity.rotateY(math.pi/2).mul(mat.getOrDefault(mat4Identity));
                                }
                            ];
                            children = [
                                smallCube
                            ];
                        },
                        Subscene {
                            uniformValues = [
                                Uniform<Matrix4> {
                                    name = "model";
                                    uniformValue = (Option<Matrix4> mat) =>
                                        mat4Identity.rotateY(math.pi).mul(mat.getOrDefault(mat4Identity));
                                }
                            ];
                            children = [
                                smallCube
                            ];
                        },
                        Subscene {
                            uniformValues = [
                                Uniform<Matrix4> {
                                    name = "model";
                                    uniformValue = (Option<Matrix4> mat) =>
                                        mat4Identity.rotateY(-math.pi/2).mul(mat.getOrDefault(mat4Identity));
                                }
                            ];
                            children = [
                                smallCube
                            ];
                        }
                    ];
                }
            ];
        };
    }
}