import functionalRenderer.model {
    Uniform,
    SceneRoot,
    Subscene,
    mat4Identity,
    Viewport,
    Matrix4,
    ShaderProgram
}
import demoCommon.utils {
    loadShader
}
import math {
    math
}
import option {
    Option
}
shared class Scene06() satisfies Scene {
    ShaderProgram sp = ShaderProgram(loadShader("colour_vert"), loadShader("colour_frag"));

    shared actual SceneRoot getScene(Integer time, Matrix4 view) {
//        Matrix4 usedView = Matrix4.view(Vector3(1.023, 1.17, 1.87), -0.445, 0.495);
//        Float rotation = 0.4;
        Matrix4 usedView = view;
        Float rotation = math.abs(2000 - time.modulo(4000))/2000.0 * math.pi/2;
        Matrix4 projection = Matrix4.perspective(0.1, 100.0, math.pi / 3.0);

        Subscene knob = Subscene {
            geometry = sharedGeometry.knob;
            uniformValues = [
                Uniform<Matrix4> {
                    name = "model";
                    uniformValue = (Option<Matrix4> parent) => mat4Identity.translateZ(0.05).translateX(0.35).mul(parent.getOrDefault(mat4Identity));
                }
            ];
        };

        Subscene doorBoard = Subscene {
            geometry = sharedGeometry.door;
        };

        Subscene innerDoor = Subscene {
            uniformValues = [
                Uniform<Matrix4> {
                    name = "model";
                    uniformValue = (Option<Matrix4> parent) => mat4Identity
                        .translateX(0.45)
                        .rotateY(rotation)
                        .translateX(-0.45)
                        .mul(parent.getOrDefault(mat4Identity));
                }
            ];
            children = [
                doorBoard,
                knob
            ];
        };

        Subscene doorFrame = Subscene {
            geometry = sharedGeometry.doorFrame;
        };

        Subscene door = Subscene{
            children = [
                doorFrame,
                innerDoor
            ];
            uniformValues = [
                Uniform {
                    name = "model";
                    uniformValue = mat4Identity;
                },
                Uniform {
                    name = "view";
                    uniformValue = usedView;
                },
                Uniform {
                    name = "proj";
                    uniformValue = projection;
                }
            ];
        };

        return SceneRoot {
            viewport = Viewport(0, 0, 640, 640);
            shaderProgram = sp;
            children = [
                door
            ];
        };
    }
}