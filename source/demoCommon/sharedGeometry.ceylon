import functionalRenderer.model {
    Geometry,
    ShaderAttribute,
    VertexBuffer,
    IndexBuffer
}
import functionalRenderer.model.renderSettingValues { lineStrip }

shared object sharedGeometry {
    Float frameWidth = 0.1;
    Integer verticesPerBox = 24;

    shared Geometry cube = Geometry {
        vertexBuffer = VertexBuffer(expand(boxVertices(0.0, 0.0, 0.0, 2.0, 2.0, 2.0)).sequence());
        indexBuffer = IndexBuffer(boxIndices().sequence());
        attributes = [
            ShaderAttribute("aVertexPosition", 0, 3)
        ];
    };

    shared Geometry cube2 = Geometry {
        vertexBuffer = VertexBuffer([
            -1.0, 1.0, 1.0, 0.0, 0.0, //front
            1.0, 1.0, 1.0, 1.0, 0.0,
            1.0, -1.0, 1.0, 1.0, 1.0,
            -1.0, -1.0, 1.0, 0.0, 1.0,
            -1.0, 1.0, -1.0, 0.0, 0.0, //top
            1.0, 1.0, -1.0, 1.0, 0.0,
            1.0, 1.0, 1.0, 1.0, 1.0,
            -1.0, 1.0, 1.0, 0.0, 1.0,
            -1.0, -1.0, 1.0, 0.0, 0.0, //bottom
            1.0, -1.0, 1.0, 1.0, 0.0,
            1.0, -1.0, -1.0, 1.0, 1.0,
            -1.0, -1.0, -1.0, 0.0, 1.0,
            1.0, 1.0, -1.0, 0.0, 0.0, //back
            -1.0, 1.0, -1.0, 1.0, 0.0,
            -1.0, -1.0, -1.0, 1.0, 1.0,
            1.0, -1.0, -1.0, 0.0, 1.0,
            1.0, 1.0, 1.0, 0.0, 0.0, //right
            1.0, 1.0, -1.0, 1.0, 0.0,
            1.0, -1.0, -1.0, 1.0, 1.0,
            1.0, -1.0, 1.0, 0.0, 1.0,
            -1.0, 1.0, -1.0, 0.0, 0.0, //left
            -1.0, 1.0, 1.0, 1.0, 0.0,
            -1.0, -1.0, 1.0, 1.0, 1.0,
            -1.0, -1.0, -1.0, 0.0, 1.0
        ]);
        indexBuffer = IndexBuffer([
            0, 1, 2,
            0, 2, 3,
            4, 5, 6,
            4, 6, 7,
            8, 9, 10,
            8, 10, 11,
            12, 13, 14,
            12, 14, 15,
            16, 17, 18,
            16, 18, 19,
            20, 21, 22,
            20, 22, 23
        ]);
        attributes = [
            ShaderAttribute("aVertexPosition", 0, 3), ShaderAttribute("aTexCoor", 1, 2)
        ];
    };

    shared Geometry cubeLines = Geometry {
        vertexBuffer = VertexBuffer([
            -1.0, 1.0, 1.0,
            1.0, 1.0, 1.0,
            1.0, -1.0, 1.0,
            -1.0, -1.0, 1.0,

            -1.0, 1.0, -1.0,
            1.0, 1.0, -1.0,
            1.0, -1.0, -1.0,
            -1.0, -1.0, -1.0
        ]);
        indexBuffer = IndexBuffer([
            0,
            1, 2, //front
            3, 0,
            4, 5, 1, //top
            2,
            6, 5, //right
            4,
            7, 6,//back
            7,
            3 //left
        ]);
        attributes = [
            ShaderAttribute("aVertexPosition", 0, 3)
        ];
        drawMode = lineStrip;
    };

    shared Geometry doorFrame = Geometry {
        vertexBuffer = VertexBuffer(
            expand(interleave(
                boxVertices(0.0, 1.0, 0.0, 1.0 + frameWidth, frameWidth, frameWidth * 2).chain(
                    boxVertices(0.5, 0.0, 0.0, frameWidth, 2.0 - frameWidth, frameWidth * 2)
                ).chain(
                    boxVertices(-0.5, 0.0, 0.0, frameWidth, 2.0 - frameWidth, frameWidth * 2)
                ).chain(
                    boxVertices(0.0, -1.0, 0.0, 1.0 + frameWidth, frameWidth, frameWidth * 2)
                ),
                {{0.4, 0.2, 0.0}}.repeat(verticesPerBox / 6).chain(
                    {{0.5, 0.3, 0.0}}.repeat(verticesPerBox / 6)
                ).chain(
                    {{0.5, 0.3, 0.0}}.repeat(verticesPerBox / 6)
                ).chain(
                    {{0.4, 0.2, 0.0}}.repeat(verticesPerBox / 6)
                ).chain(
                    {{0.5, 0.3, 0.0}}.repeat(verticesPerBox / 6)
                ).chain(
                    {{0.5, 0.3, 0.0}}.repeat(verticesPerBox / 6)
                ).repeat(4)
            )).sequence()
        );
        indexBuffer = IndexBuffer(
            boxIndices().chain(
                boxIndices(verticesPerBox*1)
            ).chain(
                boxIndices(verticesPerBox*2)
            ).chain(
                boxIndices(verticesPerBox*3)
            ).sequence()
        );
        attributes = [
            ShaderAttribute("aVertexPosition", 0, 3),
            ShaderAttribute("aColour", 1, 3)
        ];
    };

    shared Geometry door = Geometry {
        vertexBuffer = VertexBuffer(
            expand(interleave(
                boxVertices(0.0, 0.0, 0.0, 1.0 - frameWidth, 2.0 - frameWidth, frameWidth),
                {{1.0, 0.6, 0.3}}.repeat(verticesPerBox / 6).chain(
                    {{1.0, 0.7, 0.4}}.repeat(verticesPerBox / 6)
                ).chain(
                    {{1.0, 0.7, 0.4}}.repeat(verticesPerBox / 6)
                ).chain(
                    {{1.0, 0.6, 0.3}}.repeat(verticesPerBox / 6)
                ).chain(
                    {{1.0, 0.7, 0.4}}.repeat(verticesPerBox / 6)
                ).chain(
                    {{1.0, 0.7, 0.4}}.repeat(verticesPerBox / 6)
                )
            )).sequence()
        );
        indexBuffer = IndexBuffer(
            boxIndices().sequence()
        );
        attributes = [
            ShaderAttribute("aVertexPosition", 0, 3),
            ShaderAttribute("aColour", 1, 3)
        ];
    };

    shared Geometry knob = Geometry {
        vertexBuffer = VertexBuffer(
            expand(interleave(
                boxVertices(0.0, 0.0, 0.0, 0.05, 0.05, 0.05).chain(
                    boxVertices(-0.1, 0.0, 0.05, 0.25, 0.05, 0.05)
                ), {
                    {0.5, 0.5, 0.5}
                }.repeat(2*verticesPerBox)
            )).sequence()
        );
        indexBuffer = IndexBuffer(
            boxIndices().chain(
                boxIndices(verticesPerBox*1)
            ).sequence()
        );
        attributes = [
            ShaderAttribute("aVertexPosition", 0, 3),
            ShaderAttribute("aColour", 1, 3)
        ];
    };
}

{{Float*}*} boxVertices(Float x, Float y, Float z, Float width, Float height, Float depth) {
    Float left = x - width / 2;
    Float rigth = x + width / 2;
    Float top = y + height / 2;
    Float bottom = y - height / 2;
    Float near = z + depth / 2;
    Float far = z - depth / 2;
    return {
        {left, top, near}, //front
        {rigth, top, near},
        {rigth, bottom, near},
        {left, bottom, near},
        {left, top, far}, //top
        {rigth, top, far},
        {rigth, top, near},
        {left, top, near},
        {left, bottom, near}, //bottom
        {rigth, bottom, near},
        {rigth, bottom, far},
        {left, bottom, far},
        {rigth, top, far}, //back
        {left, top, far},
        {left, bottom, far},
        {rigth, bottom, far},
        {rigth, top, near}, //right
        {rigth, top, far},
        {rigth, bottom, far},
        {rigth, bottom, near},
        {left, top, far}, //left
        {left, top, near},
        {left, bottom, near},
        {left, bottom, far}
    };
}

{Integer*} boxIndices(Integer indexStart = 0) {
    Integer i = indexStart;
    return {
        i+0, i+1, i+2,
        i+0, i+2, i+3,
        i+4, i+5, i+6,
        i+4, i+6, i+7,
        i+8, i+9, i+10,
        i+8, i+10, i+11,
        i+12, i+13, i+14,
        i+12, i+14, i+15,
        i+16, i+17, i+18,
        i+16, i+18, i+19,
        i+20, i+21, i+22,
        i+20, i+22, i+23
    };
}

{{Float*}*} interleave({{Float*}*} first, {{Float*}*} second) {
    return zipEntries(first, second).map(({Float*} k -> {Float*} i) => k.chain(i));
}