import ceylon.file {
    parsePath,
    File
}
import ceylon.collection {
    ArrayList
}
import ceylon.regex {
    regex
}
import functionalRenderer.model {
    TextureImage,
    TextureSettings
}
import functionalRenderer.model.renderSettingValues {
    linear,
    clampToEdge
}

import joglRenderer {
    JoglTextureImageWrapper
}
import java.io {
    JFile = File
}
import webglRenderer {
    WebglTextureImageWrapper
}
native shared [String*] loadShader(String name);

native("jvm") shared [String*] loadShader(String name) {
    value filePath = parsePath("resources/shaderPrograms/" + name + ".txt");
    ArrayList<String> stringArray = ArrayList<String>();
    if(is File file = filePath.resource) {
        value reader = file.Reader();
        while(exists line = reader.readLine()) {
            stringArray.push(line + "\n");
        }
    }
    return stringArray.sequence();
}
native("js") shared [String*] loadShader(String name) {
    dynamic {
        String s = window.shaders[name];
        String withoutVersion = regex("#version[^\\n]*").replace(s, "");
        return withoutVersion.split(Character.equals('\n')).sequence();
    }
}

native shared TextureImage loadImage(String fileName);
native("jvm") shared TextureImage loadImage(String fileName){
    return JoglTextureImageWrapper(JFile("resources/textures/" + fileName));
}
native("js") shared TextureImage loadImage(String fileName){
    dynamic {
        return WebglTextureImageWrapper(window.textures[fileName]);
    }
}

shared TextureSettings clampToEdgeAndLinear = TextureSettings(linear, linear, clampToEdge, clampToEdge);