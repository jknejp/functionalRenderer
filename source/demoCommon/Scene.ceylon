import functionalRenderer.model { Matrix4, SceneRoot }

shared interface Scene {
    shared formal SceneRoot getScene(Integer time, Matrix4 view);
}