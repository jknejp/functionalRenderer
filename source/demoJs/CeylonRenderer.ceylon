import math {
    math
}
import option {
    Option
}
import functionalRenderer.model {
    Uniform,
    SceneRoot,
    Subscene,
    Matrix4,
    mat4Identity,
    Viewport,
    ShaderProgram,
    Geometry,
    VertexBuffer,
    IndexBuffer,
    ShaderAttribute
}
import functionalRenderer.rendering {
    FunctionalRenderer
}
import demoCommon.utils {
    loadShader
}
import ceylon.collection {
    ArrayList
}
object ceylonRenderer {
    ShaderProgram sp = ShaderProgram(loadShader("light_vert"), loadShader("light_frag"));
    Matrix4 projection = Matrix4(
        1.74, 0.0, 0.0, 0.0,
        0.0, 1.74, 0.0, 0.0,
        0.0, 0.0, -1.0, -1.0,
        0.0, 0.0, -0.2, 0.0
    );
    Matrix4 view = Matrix4(
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, -0.0, 0.0,
        -0.0, 0.0, 1.0, 0.0,
        -0.0, -0.0, -4.0, 1.0
    );
    Geometry teapot = loadTeapot();
    Geometry bunny = loadBunny();

    shared void render(FunctionalRenderer functionalRenderer, Integer time, Boolean changeGeometry) {
        value choosenGeometry = if(changeGeometry && time % 2 == 0) then bunny else teapot;

        value smallTeepotModel = mat4Identity
            .translateY(-0.5)
            .scale(0.3)
            .rotateY(-time/300.0)
            .translateY(0.5)
            .translateZ(1.0);

        Subscene smallTeapot = Subscene {
            geometry = choosenGeometry;
            uniformValues = [
                Uniform<Matrix4>("model", (Option<Matrix4> parent) =>
                smallTeepotModel
                    .mul(parent.getOrDefault(mat4Identity))
                ),
                Uniform<Matrix4>("normal", (Option<Matrix4> parent) =>
                smallTeepotModel
                    .inverse
                    .transpose
                    .mul(parent.getOrDefault(mat4Identity))
                )
            ];
        };
        value mainRotation = mat4Identity.rotateY(time/1000.0);

        value scene = SceneRoot {
            viewport = Viewport(0, 0, 640, 640);
            shaderProgram = sp;
            children = [
                Subscene {
                    uniformValues = [
                        Uniform {
                            name = "model";
                            uniformValue = mainRotation;
                        },
                        Uniform {
                            name = "view";
                            uniformValue = view;
                        },
                        Uniform {
                            name = "proj";
                            uniformValue = projection;
                        },
                        Uniform {
                            name = "normal";
                            uniformValue = mainRotation.inverse.transpose;
                        }
                    ];
                    geometry = choosenGeometry;
                    children = [
                        smallTeapot,
                        Subscene{
                            uniformValues = [
                                Uniform<Matrix4>("model", (Option<Matrix4> parent) =>
                                mat4Identity
                                    .rotateY(math.pi/2)
                                    .mul(parent.getOrDefault(mat4Identity))),
                                Uniform<Matrix4>("normal", (Option<Matrix4> parent) =>
                                mat4Identity
                                    .rotateY(math.pi/2)
                                    .inverse
                                    .transpose
                                    .mul(parent.getOrDefault(mat4Identity)))
                            ];
                            children = [
                                smallTeapot
                            ];
                        },
                        Subscene{
                            uniformValues = [
                                Uniform<Matrix4>("model", (Option<Matrix4> parent) =>
                                mat4Identity
                                    .rotateY(math.pi)
                                    .mul(parent.getOrDefault(mat4Identity))),
                                Uniform<Matrix4>("normal", (Option<Matrix4> parent) =>
                                mat4Identity
                                    .rotateY(math.pi)
                                    .inverse
                                    .transpose
                                    .mul(parent.getOrDefault(mat4Identity)))
                            ];
                            children = [
                                smallTeapot
                            ];
                        },
                        Subscene{
                            uniformValues = [
                                Uniform<Matrix4>("model", (Option<Matrix4> parent) =>
                                mat4Identity
                                    .rotateY(-math.pi/2)
                                    .mul(parent.getOrDefault(mat4Identity))),
                                Uniform<Matrix4>("normal", (Option<Matrix4> parent) =>
                                mat4Identity
                                    .rotateY(-math.pi/2)
                                    .inverse
                                    .transpose
                                    .mul(parent.getOrDefault(mat4Identity)))
                            ];
                            children = [
                                smallTeapot
                            ];
                        }
                    ];
                }
            ];
        };

        functionalRenderer.render(scene);
    }
}

Geometry loadBunny() {
    value obj = loadObj("bunny");
    return obj;
}

Geometry loadTeapot() {
    value obj = loadObj("wt_teapot");
    return obj;
}

Geometry loadObj(String name) {
    ArrayList<Float> vertices = ArrayList<Float>();
    ArrayList<Float> normals = ArrayList<Float>();
    ArrayList<Integer -> Integer> indices = ArrayList<Integer -> Integer>();

    String obj;
    dynamic {
        obj = window.objects[name];
    }
    {String*} lines = obj.split((Character ch) => ch == '\n' || ch == '\r');
    for(line in lines) {
        {String+} split = line.split((Character ch) => ch == ' ');
        switch (split.first)
        case ("v") {
            value newVerts = split.rest.map((String element) => parseFloat(element));
            vertices.addAll(newVerts);
        }
        case ("f") {
            for(entry in split.rest) {
                value innerSplit = entry.split((Character ch) => ch == '/');
                value first = parseInteger(innerSplit.first) - 1;
                value last = parseInteger(innerSplit.last) - 1;
                indices.add(first -> last);
            }
        }
        case ("vn") {
            value newNormals = split.rest.map((String element) => parseFloat(element));
            normals.addAll(newNormals);
        }
        else {
        }
    }

    value finalVertices = indices.flatMap((Integer vertexIndex -> Integer normalIndex) {
        return [
            vertices[vertexIndex*3 + 0] else nothing,
            vertices[vertexIndex*3 + 1] else nothing,
            vertices[vertexIndex*3 + 2] else nothing,
            normals[normalIndex*3 + 0] else nothing,
            normals[normalIndex*3 + 1] else nothing,
            normals[normalIndex*3 + 2] else nothing
        ];
    });

    return Geometry(
        VertexBuffer(finalVertices.sequence()),
        IndexBuffer((0:indices.size).sequence()),
        [
            ShaderAttribute("aVertexPosition", 0, 3),
            ShaderAttribute("aNormal", 1, 3)
        ]
    );
}

Float parseFloat(String string) {
    value parsed = Float.parse(string);


    switch (parsed)
    case(is Float) {
        return parsed;
    }
    else {
        throw Exception("Cannot parse \"" + string + "\" as float");
    }
}

Integer parseInteger(String string) {
    value parsed = Integer.parse(string);

    switch (parsed)
    case(is Integer) {
        return parsed;
    }
    else {
        throw Exception("Cannot parse \"" + string + "\" as integer");
    }
}