import functionalRenderer.rendering {
    FunctionalRenderer
}
import webglRenderer {
    WebglRenderer
}
import demoCommon {
    Scene01,
    Scene,
    Scene02,
    Scene03,
    Scene04
}
import functionalRenderer.model {
    Matrix4,
    Vector3
}
import math {
    math
}
shared void run() {
    FunctionalRenderer renderer;
    variable Integer startTime = -1;

    variable Integer lastX = -1;
    variable Integer lastY = -1;

    variable Vector3<Float> position = Vector3(0.0, 0.0, 4.0);
    variable Float pitch = 0.0;
    variable Float yaw = 0.0;

    variable Scene? scene = null;

    variable Integer frames = 0;
    variable Integer measurementStartTime = -1;

    void loadScene01(Anything() callback = () => {}) {
        downloadShader("simple_frag", void() {
            downloadShader("simple_vert", void() {
                scene = Scene01();
                callback();
            });
        });
    }

    void loadScene02(Anything() callback = () => {}) {
        downloadShader("simple_frag", void() {
            downloadShader("simple_vert", void() {
                scene = Scene02();
                callback();
            });
        });
    }

    void loadScene03(FunctionalRenderer renderer, Anything() callback = () => {}) {
        downloadShader("texCoor_frag", void() {
            downloadShader("texCoor_vert", void() {
                downloadTexture("brick.jpg", void() {
                    downloadTexture("rock.jpg", void() {
                        scene = Scene03(renderer);
                        callback();
                    });
                });
            });
        });
    }

    void loadScene04(Anything() callback = () => {}) {
        downloadShader("simple_frag", void() {
            downloadShader("simple_vert", void() {
                downloadShader("texCoor_frag", void() {
                    downloadShader("texCoor_vert", void() {
                        scene = Scene04();
                        callback();
                    });
                });
            });
        });
    }

    void attachEventListeners() {
        dynamic {
            document.body.onmousedown = void (dynamic e) {
                lastX = e.clientX;
                lastY = e.clientY;
            };
            document.body.onmouseup = void (dynamic e) {
                lastX = -1;
                lastY = -1;
            };
            document.body.onmousemove = void (dynamic e) {
                if (lastX != -1, lastY != -1) {
                    Float sensitivity = 0.005;
                    Integer x = e.clientX;
                    Integer y = e.clientY;

                    yaw = yaw + (x - lastX) * sensitivity;
                    pitch = pitch + (y - lastY) * sensitivity;
                    pitch = math.max(pitch, -math.pi/2);
                    pitch = math.min(pitch, math.pi/2);
                    lastX = x;
                    lastY = y;
                }
            };
            document.body.onkeydown = void (dynamic e) {
                Float speed = 0.2;

                if(e.keyCode == 68 || e.keyCode == 39) {
                    position = position + Vector3(math.cos(yaw) * speed, 0.0, -math.sin(yaw) * speed);
                }
                if(e.keyCode == 65 || e.keyCode == 37) {
                    position = position - Vector3(math.cos(yaw) * speed, 0.0, -math.sin(yaw) * speed);
                }
                if(e.keyCode == 87 || e.keyCode == 38) {
                    position = position + Vector3(-math.sin(yaw) * math.cos(pitch) * speed, math.sin(pitch) * speed, -math.cos(yaw) * math.cos(pitch) * speed);
                }
                if(e.keyCode == 83 || e.keyCode == 40) {
                    position = position - Vector3(-math.sin(yaw) * math.cos(pitch) * speed, math.sin(pitch) * speed, -math.cos(yaw) * math.cos(pitch) * speed);
                }
                if(e.keyCode == 16) {
                    position = position + Vector3(0.0, speed, 0.0);
                }
                if(e.keyCode == 17) {
                    position = position - Vector3(0.0, speed, 0.0);
                }
            };
        }
    }

    void addMenu(FunctionalRenderer renderer) {
        dynamic {
            dynamic menu = document.getElementById("menu");

            dynamic scene01 = document.createElement("a");
            scene01.href = "#";
            scene01.textContent = "Scene01";
            scene01.onclick = void() {
                loadScene01();
            };
            menu.appendChild(scene01);
            menu.appendChild(document.createElement("br"));

            dynamic scene02 = document.createElement("a");
            scene02.href = "#";
            scene02.textContent = "Scene02";
            scene02.onclick = void() {
                loadScene02();
            };
            menu.appendChild(scene02);
            menu.appendChild(document.createElement("br"));

            dynamic scene03 = document.createElement("a");
            scene03.href = "#";
            scene03.textContent = "Scene03";
            scene03.onclick = void() {
                loadScene03(renderer);
            };
            menu.appendChild(scene03);
            menu.appendChild(document.createElement("br"));

            dynamic scene04 = document.createElement("a");
            scene04.href = "#";
            scene04.textContent = "Scene04";
            scene04.onclick = void() {
                loadScene04();
            };
            menu.appendChild(scene04);
            menu.appendChild(document.createElement("br"));

            dynamic stop = document.createElement("a");
            stop.href = "#";
            stop.textContent = "stop";
            stop.onclick = void() {
                scene = null;
            };
            menu.appendChild(stop);
            menu.appendChild(document.createElement("br"));
        }
    }

    dynamic {
        dynamic canvas = document.getElementById("canvas");
        canvas.width = 640;
        canvas.height = 640;
        dynamic gl = canvas.getContext("webgl");
        gl.viewport(0, 0, canvas.width, canvas.height);
        renderer = FunctionalRenderer(WebglRenderer(gl));

        attachEventListeners();
        addMenu(renderer);
    }

    void render() {
        if(startTime == -1) {
            startTime = system.milliseconds;
        }
        if(measurementStartTime == -1) {
            measurementStartTime = system.milliseconds;
        }
        Matrix4 view = Matrix4.view(position, pitch, yaw);
        if(exists renderedScene = scene) {
            renderer.render(renderedScene.getScene(system.milliseconds - startTime, view));
        }
        scheduleNextRender(render);
        frames += 1;
        if(frames == 100) {
//            print(frames.float / (system.milliseconds - measurementStartTime) * 1000);
            frames = 0;
            measurementStartTime = system.milliseconds;
        }
    }

    loadScene01(render);
}

void scheduleNextRender(Anything() callback) {
    dynamic {
        requestAnimationFrame(callback);
    }
}

shared void runCeylonPerformance() {
    Integer testFrameCount = 10000;

    downloadObject("bunny", void() {
        downloadObject("wt_teapot", void() {
            downloadShader("light_vert", void() {
                downloadShader("light_frag", void() {
                    FunctionalRenderer renderer;
                    dynamic {
                        dynamic canvas = document.getElementById("canvas");
                        canvas.width = 640;
                        canvas.height = 640;
                        dynamic gl = canvas.getContext("webgl");
                        gl.viewport(0, 0, canvas.width, canvas.height);
                        renderer = FunctionalRenderer(WebglRenderer(gl));
                    }
                    Integer startTime = system.milliseconds;
                    variable Integer frames = 0;

                    void doRender() {
                        ceylonRenderer.render(renderer, system.milliseconds - startTime, false);
                        frames += 1;
                        if(frames < testFrameCount) {
                            dynamic {
                                window.setTimeout(doRender);
                            }
                        } else {
                            print((system.milliseconds - startTime).string + "ms");
                        }
                    }

                    doRender();
                });
            });
        });
    });
}

shared void runJavascriptPerformance() {
    Integer testFrameCount = 10000;

    downloadObject("bunny", void() {
        downloadObject("wt_teapot", void() {
            downloadShader("light_vert", void() {
                downloadShader("light_frag", void() {
                    dynamic gl;
                    dynamic {
                        dynamic canvas = document.getElementById("canvas");
                        canvas.width = 640;
                        canvas.height = 640;
                        gl = canvas.getContext("webgl");
                        gl.viewport(0, 0, canvas.width, canvas.height);
                    }
                    Integer startTime = system.milliseconds;
                    variable Integer frames = 0;

                    void doRender() {
                        dynamic {
                            window.javascriptRenderer.render(gl, system.milliseconds - startTime, false);
                        }
                        frames += 1;
                        if(frames < testFrameCount) {
                            dynamic {
                                window.setTimeout(doRender);
                            }
                        } else {
                            print((system.milliseconds - startTime).string + "ms");
                        }
                    }

                    doRender();
                });
            });
        });
    });
}