"Default documentation for module `demoJs`."

native("js") module demoJs "1.0.0" {
    shared import functionalRenderer "1.0.0";
    shared import webglRenderer "1.0.0";
    shared import math "1.0.0";
    import demoCommon "1.0.0";
    import ceylon.collection "1.3.2";
}
