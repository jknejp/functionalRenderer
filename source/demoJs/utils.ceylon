shared void downloadShader(String name, Anything() done) {
    dynamic {
        xhttp = XMLHttpRequest();
        xhttp.onreadystatechange = void() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                if(!exists s = window.shaders) {
                    window.shaders = dynamic [];
                }
                window.shaders[name] = xhttp.responseText;
                done();
            }
        };
        xhttp.open("GET", "shaderPrograms/" + name + ".txt", true);
        xhttp.send();
    }
}

shared void downloadObject(String name, Anything() done) {
    dynamic {
        xhttp = XMLHttpRequest();
        xhttp.onreadystatechange = void() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                if(!exists s = window.objects) {
                    window.objects = dynamic [];
                }
                window.objects[name] = xhttp.responseText;
                done();
            }
        };
        xhttp.open("GET", "objects/" + name + ".obj", true);
        xhttp.send();
    }
}

shared void downloadTexture(String name, Anything() done) {
    dynamic {
        if(!exists s = window.textures) {
            window.textures = dynamic [];
        }

        image = Image();
        image.onload = done;
        image.src = "textures/" + name;
        window.textures[name] = image;
    }
}