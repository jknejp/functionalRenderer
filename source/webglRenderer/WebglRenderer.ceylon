import functionalRenderer.rendering {
    NativeRenderer
}
import functionalRenderer.model.renderSettingValues {
    DrawMode,
    lineStrip,
    triangleFan,
    points,
    triangleStrip,
    lineLoop,
    lines,
    triangles,
    DepthFunction,
    equal,
    less,
    greaterOrEqual,
    greater,
    never,
    notEqual,
    lessOrEqual,
    off,
    always,
    TextureFilterMipmapSupport,
    repeat,
    TextureWrapFunction,
    mirroredRepeat,
    linearMipmapNearest,
    nearest,
    linear,
    clampToEdge,
    nearestMipmapNearest,
    linearMipmapLinear,
    nearestMipmapLinear
}
import functionalRenderer.model {
    ShaderProgram,
    FramebufferSetting,
    Colour,
    AnyUniform,
    ShaderAttribute,
    GpuProgram,
    Uniform,
    Texture,
    Matrix4,
    GpuVertexBuffer,
    GpuIndexBuffer,
    GpuTexture,
    TextureImage,
    RenderPass,
    GpuFramebuffer,
    Viewport,
    TextureSettings,
    TextureUsage,
    Vector4,
    Vector3,
    Vector2,
    RenderResult
}

shared class WebglRenderer(WebGLRenderingContext gl) satisfies NativeRenderer {
    shared actual void refresh(){}
    shared actual GpuProgram compileProgram(ShaderProgram shaderProgram) {
        String(String, String) concatStrings = (String s1, String s2) => s1+s2;

        WebGLShader vShader = gl.createShader(gl.\iVERTEX_SHADER);
        String vShaderSrc = shaderProgram.vertexShader.reduce(concatStrings) else "";
        gl.shaderSource(vShader, vShaderSrc);
        gl.compileShader(vShader);
        if (is Boolean compileStatus = gl.getShaderParameter(vShader, gl.\iCOMPILE_STATUS), !compileStatus) {
            print(gl.getShaderInfoLog(vShader));
        }

        WebGLShader fShader = gl.createShader(gl.\iFRAGMENT_SHADER);
        String fShaderSrc = shaderProgram.fragmentShader.reduce(concatStrings) else "";
        gl.shaderSource(fShader, fShaderSrc);
        gl.compileShader(fShader);
        if (is Boolean compileStatus = gl.getShaderParameter(fShader, gl.\iCOMPILE_STATUS), !compileStatus) {
            print(gl.getShaderInfoLog(fShader));
        }

        WebGLProgram program = gl.createProgram();
        gl.attachShader(program, vShader);
        gl.attachShader(program, fShader);

        return WebglProgramWrapper(program);
    }

    shared actual void useProgram(GpuProgram program) {
        gl.useProgram(getProgramObject(program));
    }

    shared actual void setAttributes([ShaderAttribute*] attributes, GpuProgram program) {
        value webglProgram = getProgramObject(program);
        for (att in attributes) {
            gl.bindAttribLocation(webglProgram, att.index, att.name);
        }

        gl.linkProgram(webglProgram);
        if (is Boolean linkStatus = gl.getProgramParameter(webglProgram, gl.\iLINK_STATUS), !linkStatus) {
            print(gl.getProgramInfoLog(webglProgram));
        }
    }

    shared actual void enableAttributes([ShaderAttribute*] attributes, GpuProgram program) {
        value webglProgram = getProgramObject(program);
        value stride = attributes.fold(0)(((Integer a, ShaderAttribute b) => a + b.size));
        {Integer+} offsets = attributes.scan(0)((Integer a, ShaderAttribute b) => a + b.size);
        for (att -> offset in zipEntries(attributes, offsets)) {
            gl.enableVertexAttribArray(att.index);
            gl.vertexAttribPointer(att.index, att.size, gl.\iFLOAT, false, stride * 4, offset * 4);
        }
    }

    shared actual void draw(DrawMode drawMode, Integer numberOfIndices) {
        Integer webglMode => switch(drawMode)
        case (points) gl.\iPOINTS
        case (lineStrip) gl.\iLINE_STRIP
        case (lineLoop) gl.\iLINE_LOOP
        case (lines) gl.\iLINES
        case (triangleStrip) gl.\iTRIANGLE_STRIP
        case (triangleFan) gl.\iTRIANGLE_FAN
        case (triangles) gl.\iTRIANGLES;
        gl.drawElements(webglMode, numberOfIndices, gl.\iUNSIGNED_SHORT, 0);
    }

    shared actual GpuIndexBuffer loadIndexBuffer([Integer*] indices) {
        WebGLBuffer buffer = gl.createBuffer();
        gl.bindBuffer(gl.\iELEMENT_ARRAY_BUFFER, buffer);
        gl.bufferData(gl.\iELEMENT_ARRAY_BUFFER, toJsBufferI(indices), gl.\iSTATIC_DRAW);

        return WebglIndexBufferWrapper(buffer);
    }

    shared actual void setIndexBuffer(GpuIndexBuffer buffer) {
        gl.bindBuffer(gl.\iELEMENT_ARRAY_BUFFER, getIndexBufferObject(buffer));
    }

    shared actual GpuVertexBuffer loadVertexBuffer([Float*] vertices) {
        WebGLBuffer buffer = gl.createBuffer();
        gl.bindBuffer(gl.\iARRAY_BUFFER, buffer);
        gl.bufferData(gl.\iARRAY_BUFFER, toJsBufferF(vertices), gl.\iSTATIC_DRAW);

        return WebglVertexBufferWrapper(buffer);
    }

    shared actual void setVertexBuffer(GpuVertexBuffer buffer) {
        gl.bindBuffer(gl.\iARRAY_BUFFER, getVertexBufferObject(buffer));
    }

    shared actual void setDepthBuffer(DepthFunction depthFunction) {
        Integer webglFunc = switch (depthFunction)
        case (off) -1
        case (never) gl.\iNEVER
        case (less) gl.\iLESS
        case (equal) gl.\iEQUAL
        case (notEqual) gl.\iNOTEQUAL
        case (lessOrEqual) gl.\iLEQUAL
        case (greater) gl.\iGREATER
        case (greaterOrEqual) gl.\iGEQUAL
        case (always) gl.\iALWAYS;
        if(webglFunc == -1) {
            gl.disable(gl.\iDEPTH_TEST);
        } else {
            gl.enable(gl.\iDEPTH_TEST);
            gl.depthFunc(webglFunc);
        }
    }

    shared actual GpuTexture loadTexture(TextureImage image) {
        WebGLTexture texture = gl.createTexture();
        gl.bindTexture(gl.\iTEXTURE_2D, texture);
        gl.texImage2D(gl.\iTEXTURE_2D, 0, gl.\iRGBA, gl.\iRGBA, gl.\iUNSIGNED_BYTE, getTextureImage(image));

        return WebglTextureWrapper(texture);
    }

    shared actual void bindTexture(Integer slot, GpuTexture? texture, TextureSettings settings) {
        Integer webglTextureFilter(TextureFilterMipmapSupport filter) => switch(filter)
            case (nearestMipmapNearest) gl.\iNEAREST_MIPMAP_NEAREST
            case (linearMipmapNearest) gl.\iLINEAR_MIPMAP_NEAREST
            case (nearestMipmapLinear) gl.\iNEAREST_MIPMAP_LINEAR
            case (linearMipmapLinear) gl.\iLINEAR_MIPMAP_LINEAR
            case (linear) gl.\iLINEAR
            case (nearest) gl.\iNEAREST;

        Integer webglTextureWrap(TextureWrapFunction wrap) => switch(wrap)
            case (repeat) gl.\iREPEAT
            case (clampToEdge) gl.\iCLAMP_TO_EDGE
            case (mirroredRepeat) gl.\iMIRRORED_REPEAT;

        gl.activeTexture(gl.\iTEXTURE0 + slot);
        if(exists texture) {
            gl.bindTexture(gl.\iTEXTURE_2D, getTextureObject(texture));
            gl.texParameteri(gl.\iTEXTURE_2D, gl.\iTEXTURE_MIN_FILTER, webglTextureFilter(settings.minFilter));
            gl.texParameteri(gl.\iTEXTURE_2D, gl.\iTEXTURE_MAG_FILTER, webglTextureFilter(settings.magFilter));
            gl.texParameteri(gl.\iTEXTURE_2D, gl.\iTEXTURE_WRAP_S, webglTextureWrap(settings.wrapS));
            gl.texParameteri(gl.\iTEXTURE_2D, gl.\iTEXTURE_WRAP_T, webglTextureWrap(settings.wrapT));
        } else {
            gl.bindTexture(gl.\iTEXTURE_2D, null);
        }
    }

    shared actual void releaseTextures([Integer*] ids) {}
    shared actual GpuFramebuffer createFrameBuffer(FramebufferSetting setting) {
        WebGLFramebuffer framebuffer = gl.createFramebuffer();
        gl.bindFramebuffer(gl.\iFRAMEBUFFER, framebuffer);

        WebGLTexture texture = gl.createTexture();
        gl.bindTexture(gl.\iTEXTURE_2D, texture);
        gl.texImage2D(gl.\iTEXTURE_2D, 0, gl.\iRGBA, setting.width, setting.height, 0, gl.\iRGBA, gl.\iUNSIGNED_BYTE, null);
        gl.framebufferTexture2D(gl.\iFRAMEBUFFER, gl.\iCOLOR_ATTACHMENT0, gl.\iTEXTURE_2D, texture, 0);

        WebGLRenderbuffer depthBuffer = gl.createRenderbuffer();
        gl.bindRenderbuffer(gl.\iRENDERBUFFER, depthBuffer);
        gl.renderbufferStorage(gl.\iRENDERBUFFER, gl.\iDEPTH_COMPONENT16, setting.width, setting.height);
        gl.framebufferRenderbuffer(gl.\iFRAMEBUFFER, gl.\iDEPTH_ATTACHMENT, gl.\iRENDERBUFFER, depthBuffer);

        if (gl.checkFramebufferStatus(gl.\iFRAMEBUFFER) != gl.\iFRAMEBUFFER_COMPLETE) {
            print("Frame buffer error: " + gl.checkFramebufferStatus(gl.\iFRAMEBUFFER).string);
        }

        return WebglFramebufferWrapper(framebuffer, WebglTextureWrapper(texture), setting);
    }

    shared actual void setFrameBuffer(GpuFramebuffer? framebuffer) {
        if(exists framebuffer) {
            gl.bindFramebuffer(gl.\iFRAMEBUFFER, getFramebufferObject(framebuffer));
        } else {
            gl.bindFramebuffer(gl.\iFRAMEBUFFER, null);
        }
    }
    shared actual void clear(Colour c) {
        gl.clearColor(c.red, c.green, c.blue, c.alpha);
        gl.clear(gl.\iCOLOR_BUFFER_BIT.or(gl.\iDEPTH_BUFFER_BIT));
    }
    shared actual void bindUniform(AnyUniform uniform, GpuProgram program) {
        WebGLProgram programObject = getProgramObject(program);
        switch (uniform)
        case (is Uniform<Matrix4>) {
            WebGLUniformLocation location = gl.getUniformLocation(programObject, uniform.name);
            gl.uniformMatrix4fv(location, false, toJsBufferF(uniform.val().transpose.sequence));
        }
        case (is Uniform<Integer>) {
            WebGLUniformLocation location = gl.getUniformLocation(programObject, uniform.name);
            gl.uniform1i(location, uniform.val());
        }
        case (is Uniform<Vector2<Integer>>) {
            Vector2<Integer> val = uniform.val();
            WebGLUniformLocation location = gl.getUniformLocation(programObject, uniform.name);
            gl.uniform2i(location, val.x, val.y);
        }
        case (is Uniform<Vector3<Integer>>) {
            WebGLUniformLocation location = gl.getUniformLocation(programObject, uniform.name);
            Vector3<Integer> val = uniform.val();
            gl.uniform3i(location, val.x, val.y, val.z);
        }
        case (is Uniform<Vector4<Integer>>) {
            WebGLUniformLocation location = gl.getUniformLocation(programObject, uniform.name);
            Vector4<Integer> val = uniform.val();
            gl.uniform4i(location, val.x, val.y, val.z, val.w);
        }
        case (is Uniform<Float>) {
            WebGLUniformLocation location = gl.getUniformLocation(programObject, uniform.name);
            gl.uniform1f(location, uniform.val());
        }
        case (is Uniform<Vector2<Float>>) {
            WebGLUniformLocation location = gl.getUniformLocation(programObject, uniform.name);
            Vector2<Float> val = uniform.val();
            gl.uniform2f(location, val.x, val.y);
        }
        case (is Uniform<Vector3<Float>>) {
            WebGLUniformLocation location = gl.getUniformLocation(programObject, uniform.name);
            Vector3<Float> val = uniform.val();
            gl.uniform3f(location, val.x, val.y, val.z);
        }
        case (is Uniform<Vector4<Float>>) {
            WebGLUniformLocation location = gl.getUniformLocation(programObject, uniform.name);
            Vector4<Float> val = uniform.val();
            gl.uniform4f(location, val.x, val.y, val.z, val.w);
        }
        case (is Uniform<TextureUsage>) {
        }
        case (is Uniform<RenderPass>) {
        }
    }

    shared actual void viewport(Viewport viewport) {
        gl.viewport(viewport.x, viewport.y, viewport.width, viewport.height);
    }

    shared actual RenderResult readImage() {
        ArrayBuffer pixels;
        dynamic {
            Integer w;
            Integer h;
            dynamic viewport =  gl.getParameter(gl.\iVIEWPORT);
            w = viewport[2];
            h = viewport[3];
            pixels = Uint8Array(w * h * 4);

            gl.readPixels(0, 0, w, h, gl.\iRGBA, gl.\iUNSIGNED_BYTE, pixels);
        }

        return WebglRenderResultWrapper(pixels);
    }
}

WebGLProgram getProgramObject(GpuProgram program) {
    if(is WebglProgramWrapper program) {
        return program.program;
    } else {
        throw Exception("Program object must be of type 'WebglProgramWrapper'");
    }
}

WebGLBuffer getVertexBufferObject(GpuVertexBuffer buffer) {
    if(is WebglVertexBufferWrapper buffer) {
        return buffer.buffer;
    } else {
        throw Exception("Program object must be of type 'WebglVertexBufferWrapper'");
    }
}

WebGLBuffer getIndexBufferObject(GpuIndexBuffer buffer) {
    if(is WebglIndexBufferWrapper buffer) {
        return buffer.buffer;
    } else {
        throw Exception("Program object must be of type 'WebglIndexBufferWrapper'");
    }
}

WebGLTexture getTextureObject(GpuTexture texture) {
    if(is WebglTextureWrapper texture) {
        return texture.texture;
    } else {
        throw Exception("Program object must be of type 'WebglTextureWrapper'");
    }
}

Image2D getTextureImage(TextureImage image) {
    if(is WebglTextureImageWrapper image) {
        return image.image;
    } else {
        throw Exception("Program object must be of type 'WebglTextureImageWrapper'");
    }
}

WebGLFramebuffer getFramebufferObject(GpuFramebuffer buf) {
    if(is WebglFramebufferWrapper buf) {
        return buf.framebuffer;
    } else {
        throw Exception("Program object must be of type 'WebglFramebufferWrapper'");
    }
}

ArrayBuffer toJsBufferI([Integer*] ceylonSequence) {
    dynamic {
        dynamic jsArray = dynamic [*ceylonSequence];
        return Uint16Array(jsArray);
    }
}

ArrayBuffer toJsBufferF([Float*] ceylonSequence) {
    dynamic {
        dynamic jsArray = dynamic [*ceylonSequence];
        return Float32Array(jsArray);
    }
}