import functionalRenderer.model {
    GpuTexture,
    FramebufferSetting,
    GpuFramebuffer
}
class WebglFramebufferWrapper(shared WebGLFramebuffer framebuffer, shared GpuTexture tex, shared FramebufferSetting framebufferSettings)
    extends GpuFramebuffer(tex, framebufferSettings) {
}