import functionalRenderer.model {
    GpuProgram
}
shared dynamic WebGLProgram {}
shared dynamic WebGLShader {}
shared dynamic WebGLBuffer {}
shared dynamic WebGLUniformLocation {}
shared dynamic WebGLTexture {}
shared dynamic WebGLFramebuffer {}
shared dynamic WebGLRenderbuffer {}

shared dynamic Image2D {}


shared dynamic WebGLRenderingContext {
    shared formal Integer \iVERTEX_SHADER;
    shared formal Integer \iFRAGMENT_SHADER;
    shared formal Integer \iFLOAT;
    shared formal Integer \iUNSIGNED_SHORT;
    shared formal Integer \iUNSIGNED_INT;
    shared formal Integer \iUNSIGNED_BYTE;
    shared formal Integer \iPOINTS;
    shared formal Integer \iLINE_STRIP;
    shared formal Integer \iLINE_LOOP;
    shared formal Integer \iLINES;
    shared formal Integer \iTRIANGLE_STRIP;
    shared formal Integer \iTRIANGLE_FAN;
    shared formal Integer \iTRIANGLES;
    shared formal Integer \iELEMENT_ARRAY_BUFFER;
    shared formal Integer \iARRAY_BUFFER;
    shared formal Integer \iSTATIC_DRAW;
    shared formal Integer \iDEPTH_TEST;
    shared formal Integer \iNEVER;
    shared formal Integer \iLESS;
    shared formal Integer \iEQUAL;
    shared formal Integer \iNOTEQUAL;
    shared formal Integer \iLEQUAL;
    shared formal Integer \iGREATER;
    shared formal Integer \iGEQUAL;
    shared formal Integer \iALWAYS;
    shared formal Integer \iCOLOR_BUFFER_BIT;
    shared formal Integer \iDEPTH_BUFFER_BIT;
    shared formal Integer \iCOMPILE_STATUS;
    shared formal Integer \iLINK_STATUS;
    shared formal Integer \iTEXTURE_2D;
    shared formal Integer \iRGBA;
    shared formal Integer \iTEXTURE_MAG_FILTER;
    shared formal Integer \iTEXTURE_MIN_FILTER;
    shared formal Integer \iTEXTURE_WRAP_S;
    shared formal Integer \iTEXTURE_WRAP_T;
    shared formal Integer \iREPEAT;
    shared formal Integer \iCLAMP_TO_EDGE;
    shared formal Integer \iMIRRORED_REPEAT;
    shared formal Integer \iNEAREST_MIPMAP_NEAREST;
    shared formal Integer \iLINEAR_MIPMAP_NEAREST;
    shared formal Integer \iNEAREST_MIPMAP_LINEAR;
    shared formal Integer \iLINEAR_MIPMAP_LINEAR;
    shared formal Integer \iLINEAR;
    shared formal Integer \iNEAREST;
    shared formal Integer \iTEXTURE0;
    shared formal Integer \iFRAMEBUFFER;
    shared formal Integer \iFRAMEBUFFER_COMPLETE;
    shared formal Integer \iCOLOR_ATTACHMENT0;
    shared formal Integer \iDEPTH_COMPONENT;
    shared formal Integer \iDEPTH_COMPONENT16;
    shared formal Integer \iDEPTH_ATTACHMENT;
    shared formal Integer \iRENDERBUFFER;
    shared formal Integer \iVIEWPORT;

    shared formal WebGLShader createShader(Integer type);
    shared formal void shaderSource(WebGLShader shader, String source);
    shared formal void compileShader(WebGLShader shader);
    shared formal Boolean|Integer getShaderParameter(WebGLShader shader, Integer parameterName);
    shared formal String getShaderInfoLog(WebGLShader shader);
    shared formal WebGLProgram createProgram();
    shared formal void attachShader(WebGLProgram program, WebGLShader shader);
    shared formal void linkProgram(WebGLProgram program);
    shared formal void useProgram(WebGLProgram program);
    shared formal Boolean|Integer getProgramParameter(WebGLProgram program, Integer parameterName);
    shared formal String getProgramInfoLog(WebGLProgram program);
    shared formal void bindAttribLocation(WebGLProgram program, Integer index, String name);
    shared formal void enableVertexAttribArray(Integer index);
    shared formal WebGLUniformLocation getUniformLocation(WebGLProgram program, String name);
    shared formal void uniformMatrix4fv(WebGLUniformLocation location, Boolean transpose, ArrayBuffer data);
    shared formal void uniform1i(WebGLUniformLocation location, Integer val);
    shared formal void uniform2i(WebGLUniformLocation location, Integer val1, Integer val2);
    shared formal void uniform3i(WebGLUniformLocation location, Integer val1, Integer val2, Integer val3);
    shared formal void uniform4i(WebGLUniformLocation location, Integer val1, Integer val2, Integer val3, Integer val4);
    shared formal void uniform1f(WebGLUniformLocation location, Float val);
    shared formal void uniform2f(WebGLUniformLocation location, Float val1, Float val2);
    shared formal void uniform3f(WebGLUniformLocation location, Float val1, Float val2, Float val3);
    shared formal void uniform4f(WebGLUniformLocation location, Float val1, Float val2, Float val3, Float val4);
    shared formal void vertexAttribPointer(Integer index, Integer size, Integer type, Boolean normalized, Integer stride, Integer offset);
    shared formal void drawElements(Integer mode, Integer count, Integer type, Integer offset);
    shared formal WebGLBuffer createBuffer();
    shared formal void bindBuffer(Integer target, WebGLBuffer buffer);
    shared formal void bufferData(Integer target, ArrayBuffer data, Integer usage);
    shared formal void clearColor(Float red, Float green, Float blue, Float alpha);
    shared formal void enable(Integer cap);
    shared formal void disable(Integer cap);
    shared formal void depthFunc(Integer func);
    shared formal void clear(Integer mask);
    shared formal WebGLTexture createTexture();
    shared formal void bindTexture(Integer target, WebGLTexture? texture);
    shared formal void activeTexture(Integer slot);
    shared formal void texImage2D(Integer target, Integer level, Integer internalformat, Integer formatOrWidth, Integer typeOrHeight, Image2D|Integer pixelsOrBorder, Integer format = -1, Integer type = -1, Image2D? pixels = null);
    shared formal void texParameteri(Integer target, Integer pname, Integer param);
    shared formal WebGLFramebuffer createFramebuffer();
    shared formal void bindFramebuffer(Integer target, WebGLFramebuffer? framebuffer);
    shared formal void framebufferTexture2D(Integer target, Integer attachement, Integer textarget, WebGLTexture texture, Integer level);
    shared formal Integer checkFramebufferStatus(Integer target);
    shared formal WebGLRenderbuffer createRenderbuffer();
    shared formal void bindRenderbuffer(Integer target, WebGLRenderbuffer renderbuffer);
    shared formal void renderbufferStorage(Integer target, Integer internalFormat, Integer width, Integer height);
    shared formal void framebufferRenderbuffer(Integer target, Integer attachment, Integer renderbuffertarget, WebGLRenderbuffer renderbuffer);
    shared formal void viewport(Integer x, Integer y, Integer width, Integer heigth);
    shared formal ArrayBuffer getParameter(Integer pname);
    shared formal void readPixels(Integer x, Integer y, Integer width, Integer height, Integer format, Integer type, ArrayBuffer pixels);
//    shared formal
//    shared formal
//    shared formal
//    shared formal
//    shared formal
//    shared formal
}

shared dynamic ArrayBuffer {}
