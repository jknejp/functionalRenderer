import java.io {
    File
}
import functionalRenderer.model {
    TextureImage
}
shared class JoglTextureImageWrapper(shared File file) satisfies TextureImage {
}