import functionalRenderer.model {
    GpuIndexBuffer
}
class JoglIndexBufferWrapper(shared Integer id, JoglRenderer renderer) extends Finalizable() satisfies GpuIndexBuffer {
    shared actual void finalize() {
        renderer.scheduleBufferRelease(id);
    }
}