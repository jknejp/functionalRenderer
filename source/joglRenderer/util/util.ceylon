import java.lang {
    IntArray,
    ObjectArray,
    JString = String,
    FloatArray
}

shared IntArray toIntArray([Integer*] ceylonSeqence) {
    IntArray javaArray = IntArray(ceylonSeqence.size);

    for(index -> val in ceylonSeqence.indexed) {
        javaArray.set(index, val);
    }

    return javaArray;
}

shared FloatArray toFloatArray([Float*] ceylonSeqence) {
    FloatArray javaArray = FloatArray(ceylonSeqence.size);

    for(index -> val in ceylonSeqence.indexed) {
        javaArray.set(index, val);
    }

    return javaArray;
}

shared ObjectArray<JString> toStringArray([String*] ceylonSeqence) {
    ObjectArray<JString> javaArray = ObjectArray<JString>(ceylonSeqence.size);

    for(index -> val in ceylonSeqence.indexed) {
        javaArray.set(index, JString(val));
    }

    return javaArray;
}

