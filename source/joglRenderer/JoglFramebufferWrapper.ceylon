import functionalRenderer.model {
    GpuTexture,
    GpuFramebuffer,
    FramebufferSetting
}
class JoglFramebufferWrapper(shared Integer framebufferId, shared GpuTexture tex, shared FramebufferSetting framebufferSettings)
        extends GpuFramebuffer(tex, framebufferSettings) {
}