import functionalRenderer.model {
    ShaderProgram,
    Texture,
    FramebufferSetting,
    Colour,
    Uniform,
    AnyUniform,
    Matrix4,
    ShaderAttribute,
    GpuProgram,
    GpuVertexBuffer,
    GpuIndexBuffer,
    GpuTexture,
    TextureImage,
    RenderPass,
    GpuFramebuffer,
    Viewport,
    TextureSettings,
    TextureUsage,
    Vector2,
    Vector3,
    Vector4,
    RenderResult
}
import functionalRenderer.model.renderSettingValues {
    DrawMode,
    points,
    lineStrip,
    lineLoop,
    triangleStrip,
    lines,
    triangleFan,
    triangles,
    DepthFunction,
    off,
    never,
    less,
    equal,
    notEqual,
    lessOrEqual,
    greater,
    greaterOrEqual,
    always,
    TextureFilterMipmapSupport,
    nearestMipmapNearest,
    linearMipmapNearest,
    nearestMipmapLinear,
    linearMipmapLinear,
    linear,
    nearest,
    TextureWrapFunction,
    repeat,
    clampToEdge,
    mirroredRepeat
}
import com.jogamp.opengl {
    GL2ES2
}
import java.lang{IntArray,
    ByteArray, JString = String}
import joglRenderer.util{
    toStringArray,
    toIntArray,
    toFloatArray
}
import com.jogamp.common.nio {
    Buffers {newDirectIntBuffer, newDirectFloatBuffer}
}
import functionalRenderer.rendering {
    NativeRenderer
}
import com.jogamp.opengl.util.texture {
    JoglTexture = Texture,
    TextureIO{newTexture = newTexture}
}
import java.io {
    File,
    IOException
}
import java.nio {
    ByteBuffer
}
import java.awt.image {
    BufferedImage,
    DataBufferInt
}
import javax.imageio {
    ImageIO
}

shared class JoglRenderer(GL2ES2 gl) satisfies NativeRenderer {
    variable [Integer*] texturesToDelete = [];
    variable [Integer*] buffersToDelete = [];
    variable [Integer*] programsToDelete = [];

    shared actual void refresh() {
        if(texturesToDelete.size > 0) {
            releaseTextures(texturesToDelete);
            texturesToDelete = [];
        }
        if(buffersToDelete.size > 0) {
            releaseBuffers(buffersToDelete);
            buffersToDelete = [];
        }
        if(programsToDelete.size > 0) {
            releasePrograms(programsToDelete);
            programsToDelete = [];
        }
    }

    shared actual GpuProgram compileProgram(ShaderProgram shaderProgram) {
        Integer vShader = gl.glCreateShader(gl.\iGL_VERTEX_SHADER);
        [String*] vShaderSrc = shaderProgram.vertexShader;
        [Integer*] vShaderLengths = [for (line in vShaderSrc) line.size];
        gl.glShaderSource(vShader, vShaderSrc.size, toStringArray(vShaderSrc), toIntArray(vShaderLengths), 0);
        gl.glCompileShader(vShader);
        printShaderError(gl, vShader, "Error compiling vertex shader");

        Integer fShader = gl.glCreateShader(gl.\iGL_FRAGMENT_SHADER);
        [String*] fShaderSrc = shaderProgram.fragmentShader;
        [Integer*] fShaderLengths = [for (line in fShaderSrc) line.size];
        gl.glShaderSource(fShader, fShaderSrc.size, toStringArray(fShaderSrc), toIntArray(fShaderLengths), 0);
        gl.glCompileShader(fShader);
        printShaderError(gl, fShader, "Error compiling fragment shader");

        Integer programId = gl.glCreateProgram();
        gl.glAttachShader(programId, vShader);
        gl.glAttachShader(programId, fShader);

        return JoglProgramWrapper(programId, this);
    }

    shared actual void useProgram(GpuProgram program) {
        gl.glUseProgram(getProgramId(program));
        printError(gl, "Opengl e rror after using program");
    }

    shared actual void setAttributes([ShaderAttribute*] attributes, GpuProgram program) {
        value programId = getProgramId(program);
        for (att in attributes) {
            gl.glBindAttribLocation(programId, att.index, att.name);
        }

        gl.glLinkProgram(programId);
        printError(gl, "Opengl error after linking program");
        printProgramError(gl, programId, "Error linking program");
    }

    shared actual void enableAttributes([ShaderAttribute*] attributes, GpuProgram program) {
        value stride = attributes.fold(0)(((Integer a, ShaderAttribute b) => a + b.size));
        {Integer+} offsets = attributes.scan(0)((Integer a, ShaderAttribute b) => a + b.size);
        for (att -> offset in zipEntries(attributes, offsets)) {
            gl.glEnableVertexAttribArray(att.index);
            gl.glVertexAttribPointer(att.index, att.size, gl.\iGL_FLOAT, false, stride * 4, offset * 4);
        }
    }

    shared actual void draw(DrawMode drawMode, Integer numberOFIndices) {
        printError(gl, "Opengl error before drawing triangles");
        Integer joglMode => switch(drawMode)
            case (points) gl.\iGL_POINTS
            case (lineStrip) gl.\iGL_LINE_STRIP
            case (lineLoop) gl.\iGL_LINE_LOOP
            case (lines) gl.\iGL_LINES
            case (triangleStrip) gl.\iGL_TRIANGLE_STRIP
            case (triangleFan) gl.\iGL_TRIANGLE_FAN
            case (triangles) gl.\iGL_TRIANGLES;
        gl.glDrawElements(joglMode, numberOFIndices, gl.\iGL_UNSIGNED_INT, 0);
        printError(gl, "Opengl error after drawing triangles");
    }

    shared actual GpuIndexBuffer loadIndexBuffer([Integer*] indices) {
        IntArray bufferId = IntArray(1);
        gl.glGenBuffers(1, bufferId, 0);
        gl.glBindBuffer(gl.\iGL_ELEMENT_ARRAY_BUFFER, bufferId.get(0));
        value data = newDirectIntBuffer(toIntArray(indices));
        gl.glBufferData(gl.\iGL_ELEMENT_ARRAY_BUFFER, indices.size * 4, data, gl.\iGL_STATIC_DRAW);

        return JoglIndexBufferWrapper(bufferId.get(0), this);
    }

    shared actual void setIndexBuffer(GpuIndexBuffer buffer) {
        gl.glBindBuffer(gl.\iGL_ELEMENT_ARRAY_BUFFER, getIndexBufferId(buffer));
    }

    shared actual GpuVertexBuffer loadVertexBuffer([Float*] vertices) {
        IntArray bufferId = IntArray(1);
        gl.glGenBuffers(1, bufferId, 0);
        gl.glBindBuffer(gl.\iGL_ARRAY_BUFFER, bufferId.get(0));
        value data = newDirectFloatBuffer(toFloatArray(vertices));
        gl.glBufferData(gl.\iGL_ARRAY_BUFFER, vertices.size * 4, data, gl.\iGL_STATIC_DRAW);

        return JoglVertexBufferWrapper(bufferId.get(0), this);
    }

    shared actual void setVertexBuffer(GpuVertexBuffer buffer) {
        gl.glBindBuffer(gl.\iGL_ARRAY_BUFFER, getVertexBufferId(buffer));
    }

    shared actual void setDepthBuffer(DepthFunction depthFunction) {
        Integer joglFunc = switch (depthFunction)
            case (off) -1
            case (never) gl.\iGL_NEVER
            case (less) gl.\iGL_LESS
            case (equal) gl.\iGL_EQUAL
            case (notEqual) gl.\iGL_NOTEQUAL
            case (lessOrEqual) gl.\iGL_LEQUAL
            case (greater) gl.\iGL_GREATER
            case (greaterOrEqual) gl.\iGL_GEQUAL
            case (always) gl.\iGL_ALWAYS;
        if(joglFunc == -1) {
            gl.glDisable(gl.\iGL_DEPTH_TEST);
        } else {
            gl.glEnable(gl.\iGL_DEPTH_TEST);
            gl.glDepthFunc(joglFunc);
        }
    }

    shared actual void clear(Colour c) {
        gl.glClearColor(c.red, c.green, c.blue, c.alpha);
        gl.glClear(gl.\iGL_COLOR_BUFFER_BIT.or(gl.\iGL_DEPTH_BUFFER_BIT));
    }

    shared actual GpuTexture loadTexture(TextureImage image) {
        JoglTexture joglTexture = newTexture(getImageFile(image), false);
        joglTexture.bind(gl);
        return JoglTextureWrapper(joglTexture.getTextureObject(gl), this);
    }

    shared actual void bindTexture(Integer slot, GpuTexture? texture, TextureSettings settings) {
        Integer joglTextureFilter(TextureFilterMipmapSupport filter) => switch(filter)
            case (nearestMipmapNearest) gl.\iGL_NEAREST_MIPMAP_NEAREST
            case (linearMipmapNearest) gl.\iGL_LINEAR_MIPMAP_NEAREST
            case (nearestMipmapLinear) gl.\iGL_NEAREST_MIPMAP_LINEAR
            case (linearMipmapLinear) gl.\iGL_LINEAR_MIPMAP_LINEAR
            case (linear) gl.\iGL_LINEAR
            case (nearest) gl.\iGL_NEAREST;

        Integer joglTextureWrap(TextureWrapFunction wrap) => switch(wrap)
            case (repeat) gl.\iGL_REPEAT
            case (clampToEdge) gl.\iGL_CLAMP_TO_EDGE
            case (mirroredRepeat) gl.\iGL_MIRRORED_REPEAT;

        gl.glActiveTexture(gl.\iGL_TEXTURE0 + slot);
        if(exists texture) {
            gl.glBindTexture(gl.\iGL_TEXTURE_2D, getTextureId(texture));
            gl.glTexParameteri(gl.\iGL_TEXTURE_2D, gl.\iGL_TEXTURE_MIN_FILTER, joglTextureFilter(settings.minFilter));
            gl.glTexParameteri(gl.\iGL_TEXTURE_2D, gl.\iGL_TEXTURE_MAG_FILTER, joglTextureFilter(settings.magFilter));
            gl.glTexParameteri(gl.\iGL_TEXTURE_2D, gl.\iGL_TEXTURE_WRAP_S, joglTextureWrap(settings.wrapS));
            gl.glTexParameteri(gl.\iGL_TEXTURE_2D, gl.\iGL_TEXTURE_WRAP_T, joglTextureWrap(settings.wrapT));
        } else {
            gl.glBindTexture(gl.\iGL_TEXTURE_2D, 0);
        }
    }

    shared actual void releaseTextures([Integer*] ids) {
        if(ids.size > 0) {
            print("Releasing textures: " + ids.string);
            gl.glDeleteTextures(ids.size, toIntArray(ids), 0);
        }
    }

    shared void releaseBuffers([Integer*] ids) {
        if(ids.size > 0) {
            print("Releasing buffers: " + ids.string);
            gl.glDeleteBuffers(ids.size, toIntArray(ids), 0);
        }
    }

    shared void releasePrograms([Integer*] ids) {
        for(id in ids) {
            print("Releasing program: " + id.string);
            gl.glDeleteProgram(id);
        }
    }

    shared actual void setFrameBuffer(GpuFramebuffer? framebuffer) {
        if(exists framebuffer) {
            gl.glBindFramebuffer(gl.\iGL_FRAMEBUFFER, getFramebuferId(framebuffer));
        } else {
            gl.glBindFramebuffer(gl.\iGL_FRAMEBUFFER, 0);
        }
    }

    shared actual GpuFramebuffer createFrameBuffer(FramebufferSetting setting){
        if(gl.glGetError() !=0){process.writeErrorLine("Error before setFrameBuffer");}

        IntArray frameBufId = IntArray(1);
        gl.glGenFramebuffers(1, frameBufId, 0);
        gl.glBindFramebuffer(gl.\iGL_FRAMEBUFFER, frameBufId.get(0));

        IntArray texId = IntArray(1);
        gl.glGenTextures(1, texId, 0);

        gl.glBindTexture(gl.\iGL_TEXTURE_2D, texId.get(0));
        gl.glTexImage2D(gl.\iGL_TEXTURE_2D, 0, gl.\iGL_RGBA, setting.width, setting.height, 0, gl.\iGL_RGBA, gl.\iGL_UNSIGNED_BYTE, null);
        gl.glFramebufferTexture2D(gl.\iGL_FRAMEBUFFER, gl.\iGL_COLOR_ATTACHMENT0, gl.\iGL_TEXTURE_2D, texId.get(0), 0);

        IntArray depthBuffId = IntArray(1);
        gl.glGenTextures(1, depthBuffId, 0);
        gl.glBindTexture(gl.\iGL_TEXTURE_2D, depthBuffId.get(0));
        gl.glTexImage2D(gl.\iGL_TEXTURE_2D, 0, gl.\iGL_DEPTH_COMPONENT32, setting.width, setting.height, 0, gl.\iGL_DEPTH_COMPONENT, gl.\iGL_FLOAT, null);
        gl.glFramebufferTexture2D(gl.\iGL_FRAMEBUFFER, gl.\iGL_DEPTH_ATTACHMENT, gl.\iGL_TEXTURE_2D, depthBuffId.get(0), 0);

        Integer status = gl.glCheckFramebufferStatus(gl.\iGL_FRAMEBUFFER);
        if(status == gl.\iGL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT){
            process.writeErrorLine("!!!!Incomplete attachment!!!!");
        } else if (status == gl.\iGL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS) {
            process.writeErrorLine("!!!!Incomplete dimensions!!!!");
        } else if (status == gl.\iGL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT) {
            process.writeErrorLine("!!!!Incomplete missing attachment!!!!");
        } else if (status == gl.\iGL_FRAMEBUFFER_UNSUPPORTED) {
            process.writeErrorLine("!!!!Framebuffer unsopported!!!!");
        }

        if(gl.glGetError() !=0){process.writeError("Error after setFrameBuffer");}
        return JoglFramebufferWrapper(frameBufId.get(0), JoglTextureWrapper(texId.get(0), this), setting);
    }

    shared actual void bindUniform(AnyUniform uniform, GpuProgram program) {
        Integer programId = getProgramId(program);
        switch (uniform)
        case (is Uniform<Matrix4>) {
            Integer location = gl.glGetUniformLocation(programId, uniform.name);
            gl.glUniformMatrix4fv(location, 1, true, newDirectFloatBuffer(toFloatArray(uniform.val().sequence)));
        }
        case (is Uniform<Integer>) {
            Integer location = gl.glGetUniformLocation(programId, uniform.name);
            gl.glUniform1i(location, uniform.val());
        }
        case (is Uniform<Vector2<Integer>>) {
            Integer location = gl.glGetUniformLocation(programId, uniform.name);
            Vector2<Integer> val = uniform.val();
            gl.glUniform2i(location, val.x, val.y);
        }
        case (is Uniform<Vector3<Integer>>) {
            Integer location = gl.glGetUniformLocation(programId, uniform.name);
            Vector3<Integer> val = uniform.val();
            gl.glUniform3i(location, val.x, val.y, val.z);
        }
        case (is Uniform<Vector4<Integer>>) {
            Integer location = gl.glGetUniformLocation(programId, uniform.name);
            Vector4<Integer> val = uniform.val();
            gl.glUniform4i(location, val.x, val.y, val.z, val.w);
        }
        case (is Uniform<Float>) {
            Integer location = gl.glGetUniformLocation(programId, uniform.name);
            gl.glUniform1f(location, uniform.val());
        }
        case (is Uniform<Vector2<Float>>) {
            Integer location = gl.glGetUniformLocation(programId, uniform.name);
            Vector2<Float> val = uniform.val();
            gl.glUniform2f(location, val.x, val.y);
        }
        case (is Uniform<Vector3<Float>>) {
            Integer location = gl.glGetUniformLocation(programId, uniform.name);
            Vector3<Float> val = uniform.val();
            gl.glUniform3f(location, val.x, val.y, val.z);
        }
        case (is Uniform<Vector4<Float>>) {
            Integer location = gl.glGetUniformLocation(programId, uniform.name);
            Vector4<Float> val = uniform.val();
            gl.glUniform4f(location, val.x, val.y, val.z, val.w);
        }
        case (is Uniform<TextureUsage>) {
        }
        case (is Uniform<RenderPass>) {
        }
    }

    shared actual void viewport(Viewport viewport) {
        gl.glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
    }

    shared actual RenderResult readImage() {
        return JoglRenderResultWrapper(toImage());
    }

    BufferedImage toImage() {
        IntArray viewport = IntArray(4);
        gl.glGetIntegerv(gl.\iGL_VIEWPORT, viewport, 0);

        Integer w = viewport.get(2);
        Integer h = viewport.get(3);

        ByteBuffer byteBuffer = ByteBuffer.allocate(3 * w * h);
        gl.glReadPixels(0, 0, w, h, gl.\iGL_BGR, gl.\iGL_BYTE, byteBuffer);

        BufferedImage bi = BufferedImage(w, h, BufferedImage.\iTYPE_INT_ARGB);
        if(is DataBufferInt dataBuffer = bi.raster.dataBuffer) {
            IntArray pixels = dataBuffer.data;

            for (y in 0..(h - 1)) {
                for (x in 0..(w - 1)) {
                    Integer b = 2 * byteBuffer.get().unsigned;
                    Integer g = 2 * byteBuffer.get().unsigned;
                    Integer r = 2 * byteBuffer.get().unsigned;

                    Integer pixel = r.leftLogicalShift(16)
                                        .or(g.leftLogicalShift(8))
                                        .or(b)
                                        .or(#7f000000)
                                        .or(#ffffffff80000000); //Ceylon uses 64 bit integers and complains about conversion to 32bit, needs hack to flip leftmost bit.

                    pixels[(h - y - 1) * w + x] = pixel;
                }
            }
        } else {
            throw Exception("data buffer is not of type DataBufferInt");
        }

        return bi;
    }

    shared void scheduleTextureRelease(Integer id) {
        texturesToDelete = texturesToDelete.follow(id).sequence();
    }

    shared void scheduleBufferRelease(Integer id) {
        buffersToDelete = buffersToDelete.follow(id).sequence();
    }

    shared void scheduleProgramRelease(Integer id) {
        programsToDelete = programsToDelete.follow(id).sequence();
    }

    Integer getProgramId(GpuProgram program) {
        if(is JoglProgramWrapper program) {
            return program.id;
        } else {
            throw Exception("Program object must be of type 'JoglProgramWrapper'");
        }
    }

    Integer getVertexBufferId(GpuVertexBuffer buffer) {
        if(is JoglVertexBufferWrapper buffer) {
            return buffer.id;
        } else {
            throw Exception("Program object must be of type 'JoglVretexBufferWrapper'");
        }
    }

    Integer getIndexBufferId(GpuIndexBuffer buffer) {
        if(is JoglIndexBufferWrapper buffer) {
            return buffer.id;
        } else {
            throw Exception("Program object must be of type 'JoglIndexBufferWrapper'");
        }
    }

    Integer getTextureId(GpuTexture texture) {
        if(is JoglTextureWrapper texture) {
            return texture.id;
        } else {
            throw Exception("Program object must be of type 'JoglTextureWrapper'");
        }
    }

    File getImageFile(TextureImage image) {
        if(is JoglTextureImageWrapper image) {
            return image.file;
        } else {
            throw Exception("Program object must be of type 'JoglTextureImageWrapper '");
        }
    }

    Integer getFramebuferId(GpuFramebuffer framebuffer) {
        if(is JoglFramebufferWrapper framebuffer) {
            return framebuffer.framebufferId;
        } else {
            throw Exception("Program object must be of type 'JoglFramebufferWrapper '");
        }
    }

    void printError(GL2ES2 gl, String msg) {
        value error = gl.glGetError();
        if(error != 0){
            process.writeErrorLine(msg + ": " + error.string + " (0x" + Integer.format(error, 16) + ")");
        }
    }

    void printShaderError(GL2ES2 gl, Integer shader, String msg) {
        IntArray logLength = IntArray(1);
        gl.glGetShaderiv(shader, gl.\iGL_INFO_LOG_LENGTH, logLength, 0);
        if(logLength[0] > 0) {
            ByteArray errorMsg = ByteArray(logLength[0]);
            gl.glGetShaderInfoLog(shader, logLength[0], null, 0, errorMsg, 0);
            if(logLength[0] > 1) {
                process.writeErrorLine(msg);
                process.writeErrorLine(JString(errorMsg).string);
            }
        }
    }

    void printProgramError(GL2ES2 gl, Integer program, String msg) {
        IntArray logLength = IntArray(1);
        gl.glGetProgramiv(program, gl.\iGL_INFO_LOG_LENGTH, logLength, 0);
        if(logLength[0] > 0) {
            ByteArray errorMsg = ByteArray(logLength[0]);
            gl.glGetProgramInfoLog(program, logLength[0], null, 0, errorMsg, 0);
            if(logLength[0] > 1) {
                process.writeErrorLine(msg);
                process.writeErrorLine(JString(errorMsg).string);
            }
        }
    }
}