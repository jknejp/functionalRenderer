import functionalRenderer.model {
    GpuTexture
}
class JoglTextureWrapper(shared Integer id, JoglRenderer renderer) extends Finalizable() satisfies GpuTexture {
    shared actual void finalize() {
        renderer.scheduleTextureRelease(id);
    }
}