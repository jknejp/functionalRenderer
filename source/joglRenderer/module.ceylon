"Default documentation for module `joglRenderer`."
native ("jvm")
module joglRenderer "1.0.0" {
    native("jvm") import maven:"org.jogamp.gluegen:gluegen-rt" "2.3.1";
    native("jvm") shared import maven:"org.jogamp.jogl:jogl-all" "2.3.1";
    shared import functionalRenderer "1.0.0";
    shared import java.base "8";
    import java.desktop "8";
}
