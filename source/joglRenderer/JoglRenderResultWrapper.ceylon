import java.awt.image {
    BufferedImage
}
import functionalRenderer.model {
    RenderResult
}

class JoglRenderResultWrapper(BufferedImage image) satisfies RenderResult {
}