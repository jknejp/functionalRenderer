import functionalRenderer.model {
    GpuProgram
}

class JoglProgramWrapper(shared Integer id, JoglRenderer renderer) extends Finalizable() satisfies GpuProgram {
    shared actual void finalize() {
        renderer.scheduleProgramRelease(id);
    }

}