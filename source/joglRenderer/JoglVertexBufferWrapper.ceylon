import functionalRenderer.model {
    GpuVertexBuffer
}
class JoglVertexBufferWrapper(shared Integer id, JoglRenderer renderer) extends Finalizable() satisfies GpuVertexBuffer {
    shared actual void finalize() {
        renderer.scheduleBufferRelease(id);
    }
}