package joglRenderer;

public abstract class Finalizable {
    public Finalizable() {

    }

    @Override
    public abstract void finalize();
}
