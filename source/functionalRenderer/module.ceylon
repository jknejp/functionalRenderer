"Default documentation for module `functionalRenderer`."

module functionalRenderer "1.0.0" {
    import math "1.0.0";
    import ceylon.time "1.3.2";
    shared import option "1.0.0";
    native("jvm") import java.base "8";
}
