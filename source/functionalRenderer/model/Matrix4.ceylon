import math { ... }
/**
   Values are filled by rows first
 */
shared final class Matrix4 {
    shared Float v00;
    shared Float v01;
    shared Float v02;
    shared Float v03;

    shared Float v10;
    shared Float v11;
    shared Float v12;
    shared Float v13;

    shared Float v20;
    shared Float v21;
    shared Float v22;
    shared Float v23;

    shared Float v30;
    shared Float v31;
    shared Float v32;
    shared Float v33;

    shared new (
            Float v00, Float v01, Float v02, Float v03,
            Float v10, Float v11, Float v12, Float v13,
            Float v20, Float v21, Float v22, Float v23,
            Float v30, Float v31, Float v32, Float v33
            ) {
        this.v00 = v00;
        this.v01 = v01;
        this.v02 = v02;
        this.v03 = v03;

        this.v10 = v10;
        this.v11 = v11;
        this.v12 = v12;
        this.v13 = v13;

        this.v20 = v20;
        this.v21 = v21;
        this.v22 = v22;
        this.v23 = v23;

        this.v30 = v30;
        this.v31 = v31;
        this.v32 = v32;
        this.v33 = v33;
    }

    /**
       ptch and yaw are angles in radians
     */
    shared new view(Vector3<Float> eyePosition, Float pitch, Float yaw) {
        Float cosPitch = math.cos(pitch);
        Float sinPitch = math.sin(pitch);
        Float cosYaw = math.cos(yaw);
        Float sinYaw = math.sin(yaw);

        Vector3<Float> xaxis = Vector3(cosYaw, 0.0, -sinYaw);
        Vector3<Float> yaxis = Vector3(sinYaw * sinPitch, cosPitch, cosYaw * sinPitch);
        Vector3<Float> zaxis = Vector3(sinYaw * cosPitch, -sinPitch, cosPitch * cosYaw);

        this.v00 = xaxis.x;
        this.v01 = yaxis.x;
        this.v02 = zaxis.x;
        this.v03 = 0.0;

        this.v10 = xaxis.y;
        this.v11 = yaxis.y;
        this.v12 = zaxis.y;
        this.v13 = 0.0;

        this.v20 = xaxis.z;
        this.v21 = yaxis.z;
        this.v22 = zaxis.z;
        this.v23 = 0.0;

        this.v30 = -xaxis.dot(eyePosition);
        this.v31 = -yaxis.dot(eyePosition);
        this.v32 = -zaxis.dot(eyePosition);
        this.v33 = 1.0;
    }

    shared new perspective(Float near, Float far, Float fieldOfView) {
        Float scale = 1.0 / math.tan(fieldOfView/2.0);
        Float range = near - far;

        this.v00 = scale;
        this.v01 = 0.0;
        this.v02 = 0.0;
        this.v03 = 0.0;

        this.v10 = 0.0;
        this.v11 = scale;
        this.v12 = 0.0;
        this.v13 = 0.0;

        this.v20 = 0.0;
        this.v21 = 0.0;
        this.v22 = (near+far)/range;
        this.v23 = -1.0;

        this.v30 = 0.0;
        this.v31 = 0.0;
        this.v32 = 2*near*far/range;
        this.v33 = 0.0;
    }

    shared Matrix4 mul(Matrix4 other) {
        if(other == mat4Identity) {
            return this;
        } else {
            return Matrix4 (
                v00 * other.v00 + v01 * other.v10 + v02 * other.v20 + v03 * other.v30,
                v00 * other.v01 + v01 * other.v11 + v02 * other.v21 + v03 * other.v31,
                v00 * other.v02 + v01 * other.v12 + v02 * other.v22 + v03 * other.v32,
                v00 * other.v03 + v01 * other.v13 + v02 * other.v23 + v03 * other.v33,

                v10 * other.v00 + v11 * other.v10 + v12 * other.v20 + v13 * other.v30,
                v10 * other.v01 + v11 * other.v11 + v12 * other.v21 + v13 * other.v31,
                v10 * other.v02 + v11 * other.v12 + v12 * other.v22 + v13 * other.v32,
                v10 * other.v03 + v11 * other.v13 + v12 * other.v23 + v13 * other.v33,

                v20 * other.v00 + v21 * other.v10 + v22 * other.v20 + v23 * other.v30,
                v20 * other.v01 + v21 * other.v11 + v22 * other.v21 + v23 * other.v31,
                v20 * other.v02 + v21 * other.v12 + v22 * other.v22 + v23 * other.v32,
                v20 * other.v03 + v21 * other.v13 + v22 * other.v23 + v23 * other.v33,

                v30 * other.v00 + v31 * other.v10 + v32 * other.v20 + v33 * other.v30,
                v30 * other.v01 + v31 * other.v11 + v32 * other.v21 + v33 * other.v31,
                v30 * other.v02 + v31 * other.v12 + v32 * other.v22 + v33 * other.v32,
                v30 * other.v03 + v31 * other.v13 + v03 * other.v23 + v33 * other.v33
            );
        }
    }

    shared Matrix4 rotateX(Float angle) {
        return this.mul(Matrix4(
            1.0, 0.0, 0.0, 0.0,
            0.0, math.cos(angle), -math.sin(angle), 0.0,
            0.0, math.sin(angle), math.cos(angle), 0.0,
            0.0, 0.0, 0.0, 1.0
        ));
    }

    shared Matrix4 rotateY(Float angle) {
        return this.mul(Matrix4(
            math.cos(angle),0.0, math.sin(angle), 0.0,
            0.0, 1.0, 0.0, 0.0,
            -math.sin(angle), 0.0, math.cos(angle), 0.0,
            0.0, 0.0, 0.0, 1.0
        ));
    }

    shared Matrix4 rotateZ(Float angle) {
        return this.mul(Matrix4(
            math.cos(angle), -math.sin(angle), 0.0, 0.0,
            math.sin(angle), math.cos(angle), 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, 0.0, 1.0
        ));
    }

    shared Matrix4 translateX(Float distance) {
        return this.mul(Matrix4(
            1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            distance, 0.0, 0.0, 1.0
        ));
    }

    shared Matrix4 translateY(Float distance) {
        return this.mul(Matrix4(
            1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, distance, 0.0, 1.0
        ));
    }

    shared Matrix4 translateZ(Float distance) {
        return this.mul(Matrix4(
            1.0, 0.0, 0.0, 0.0,
            0.0, 1.0, 0.0, 0.0,
            0.0, 0.0, 1.0, 0.0,
            0.0, 0.0, distance, 1.0
        ));
    }

    shared Matrix4 scale(Float ratio) {
        return this.mul(Matrix4(
            ratio, 0.0, 0.0, 0.0,
            0.0, ratio, 0.0, 0.0,
            0.0, 0.0, ratio, 0.0,
            0.0, 0.0, 0.0, 1.0
        ));
    }

    shared Matrix4 transpose => Matrix4(
        v00, v10, v20, v30,
        v01, v11, v21, v31,
        v02, v12, v22, v32,
        v03, v13, v23, v33
    );

    shared Float determinant {
        value s0 = v00 * v11 - v10 * v01;
        value s1 = v00 * v12 - v10 * v02;
        value s2 = v00 * v13 - v10 * v03;
        value s3 = v01 * v12 - v11 * v02;
        value s4 = v01 * v13 - v11 * v03;
        value s5 = v02 * v13 - v12 * v03;

        value c5 = v22 * v33 - v32 * v23;
        value c4 = v21 * v33 - v31 * v23;
        value c3 = v21 * v32 - v31 * v22;
        value c2 = v20 * v33 - v30 * v23;
        value c1 = v20 * v32 - v30 * v22;
        value c0 = v20 * v31 - v30 * v21;

        return s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0;
    }

    shared Matrix4 inverse {
        value s0 = v00 * v11 - v10 * v01;
        value s1 = v00 * v12 - v10 * v02;
        value s2 = v00 * v13 - v10 * v03;
        value s3 = v01 * v12 - v11 * v02;
        value s4 = v01 * v13 - v11 * v03;
        value s5 = v02 * v13 - v12 * v03;

        value c5 = v22 * v33 - v32 * v23;
        value c4 = v21 * v33 - v31 * v23;
        value c3 = v21 * v32 - v31 * v22;
        value c2 = v20 * v33 - v30 * v23;
        value c1 = v20 * v32 - v30 * v22;
        value c0 = v20 * v31 - v30 * v21;

        value inverseDeterminant = 1 / (s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0);

        return Matrix4(
            ( v11 * c5 - v12 * c4 + v13 * c3) * inverseDeterminant,
            (-v01 * c5 + v02 * c4 - v03 * c3) * inverseDeterminant,
            ( v31 * s5 - v32 * s4 + v33 * s3) * inverseDeterminant,
            (-v21 * s5 + v22 * s4 - v23 * s3) * inverseDeterminant,
            (-v10 * c5 + v12 * c2 - v13 * c1) * inverseDeterminant,
            ( v00 * c5 - v02 * c2 + v03 * c1) * inverseDeterminant,
            (-v30 * s5 + v32 * s2 - v33 * s1) * inverseDeterminant,
            ( v20 * s5 - v22 * s2 + v23 * s1) * inverseDeterminant,
            ( v10 * c4 - v11 * c2 + v13 * c0) * inverseDeterminant,
            (-v00 * c4 + v01 * c2 - v03 * c0) * inverseDeterminant,
            ( v30 * s4 - v31 * s2 + v33 * s0) * inverseDeterminant,
            (-v20 * s4 + v21 * s2 - v23 * s0) * inverseDeterminant,
            (-v10 * c3 + v11 * c1 - v12 * c0) * inverseDeterminant,
            ( v00 * c3 - v01 * c1 + v02 * c0) * inverseDeterminant,
            (-v30 * s3 + v31 * s1 - v32 * s0) * inverseDeterminant,
            ( v20 * s3 - v21 * s1 + v22 * s0) * inverseDeterminant
        );
    }

    shared [Float+] sequence => [
        v00, v01, v02, v03,
        v10, v11, v12, v13,
        v20, v21, v22, v23,
        v30, v31, v32, v33
    ];

    shared actual String string {
        StringBuilder s = StringBuilder();
        for(index in 0..15) {
            Float? val = this.sequence[index];
            if(exists val) {
                s.append(val.string);
            } else {
                s.append("N/A");
            }
            s.append(", ");
            if(index.modulo(4) == 3) {
                s.append("\n");
            }
        }

        return s.string;
    }
}

shared Matrix4 mat4Identity = Matrix4 (
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0
);