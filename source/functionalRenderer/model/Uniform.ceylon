import option {
    Option,
    none
}
shared alias AnyUniform => Uniform<Matrix4>|Uniform<TextureUsage>|Uniform<RenderPass>|Uniform<Integer>|Uniform<Vector2<Integer>>
    |Uniform<Vector3<Integer>>|Uniform<Vector4<Integer>>|Uniform<Float>|Uniform<Vector2<Float>>|Uniform<Vector3<Float>>
    |Uniform<Vector4<Float>>;
shared class Uniform<Type>(shared String name, Type(Option<Type>)|Type uniformValue) given Type
        of Matrix4|TextureUsage|RenderPass|Integer|Vector2<Integer>|Vector3<Integer>|Vector4<Integer>
        |Float|Vector2<Float>|Vector3<Float>|Vector4<Float> {

    Type(Option<Type>) valueGetter = switch (uniformValue)
        case(is Type) ((Option<Type> v) => uniformValue)
        case(is Type(Option<Type>)) uniformValue
        else nothing; //this shouldn’t be necessary, bug?

    shared Type val(Option<Type> parentValue = none) => valueGetter(parentValue);

    shared actual String string {
        if(is Object uniform = val()) {
            return uniform.string;
        } else {
            return "<String representation missing";
        }
    }
}