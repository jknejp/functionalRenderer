shared final class Vector3<Type>(shared Type x, shared Type y, shared Type z)
        satisfies Summable<Vector3<Type>> & Invertible<Vector3<Type>>
        given Type of Integer|Float satisfies Numeric<Type>  {

    shared Type dot(Vector3<Type> that) {
        return this.x*that.x + this.y*that.y + this.z*that.z;
    }

    shared Vector3<Type> mul(Type number) {
        return Vector3<Type>(x*number, y*number, z*number);
    }
    shared actual Vector3<Type> plus(Vector3<Type> that) => Vector3<Type>(this.x+that.x, this.y+that.y, this.z+that.z);

    shared actual Vector3<Type> negated => Vector3<Type>(-x, -y, -z);

    shared actual String string => "Vector3(" + x.string + ", " + y.string + ", " + z.string + ")";
}