shared final class Vector4<Type>(shared Type x, shared Type y, shared Type z, shared Type w)
        satisfies Summable<Vector4<Type>> & Invertible<Vector4<Type>>
        given Type of Integer|Float satisfies Numeric<Type> {

    shared actual Vector4<Type> negated => Vector4(-x, -y, -z, -w);

    shared actual Vector4<Type> plus(Vector4<Type> that) => Vector4<Type>(this.x+that.x, this.y+that.y, this.z+that.z, this.w+that.w);
}