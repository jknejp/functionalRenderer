import functionalRenderer.model.renderSettingValues {
    DepthFunction,
    lessOrEqual
}
shared class SceneRoot(
    shared [Subscene*] children,
    shared ShaderProgram shaderProgram,
    shared Viewport viewport,
    shared Colour clearColour = Colour(0.0, 0.0, 0.0, 1.0),
    shared DepthFunction depthFunction = lessOrEqual
) {

}