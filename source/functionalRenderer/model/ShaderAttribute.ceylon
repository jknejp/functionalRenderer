shared class ShaderAttribute(shared String name, shared Integer index, shared Integer size){
    shared actual Boolean equals(Object other) {
        if(is ShaderAttribute other) {
            return this.name == other.name && this.index == other.index && this.size == other.size;
        } else {
            return false;
        }
    }
}