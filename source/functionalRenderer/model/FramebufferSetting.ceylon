
shared class FramebufferSetting(shared Integer width, shared Integer height) {
    shared actual Boolean equals(Object o) {
        if(is FramebufferSetting o) {
            return width == o.width && height == o.height;
        } else {
            return false;
        }
    }
}


