shared abstract class TextureFilter()
        of linear
        | nearest
        extends TextureFilterMipmapSupport()
{}

shared abstract class TextureFilterMipmapSupport()
        of TextureFilter
        | nearestMipmapNearest
        | linearMipmapNearest
        | nearestMipmapLinear
        | linearMipmapLinear
{}

shared object linear extends TextureFilter() {}
shared object nearest extends TextureFilter() {}

shared object nearestMipmapNearest extends TextureFilterMipmapSupport() {}
shared object linearMipmapNearest extends TextureFilterMipmapSupport() {}
shared object nearestMipmapLinear extends TextureFilterMipmapSupport() {}
shared object linearMipmapLinear extends TextureFilterMipmapSupport() {}