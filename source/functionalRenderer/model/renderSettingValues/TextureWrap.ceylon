shared abstract class TextureWrapFunction()
        of repeat
        | clampToEdge
        | mirroredRepeat
{}

shared object repeat extends TextureWrapFunction() {}
shared object clampToEdge extends TextureWrapFunction() {}
shared object mirroredRepeat extends TextureWrapFunction() {}
