shared abstract class DrawMode()
        of points
        | lineStrip
        | lineLoop
        | lines
        | triangleStrip
        | triangleFan
        | triangles {
}

shared object points extends DrawMode() {}
shared object lineStrip extends DrawMode() {}
shared object lineLoop extends DrawMode() {}
shared object lines extends DrawMode() {}
shared object triangleStrip extends DrawMode() {}
shared object triangleFan extends DrawMode() {}
shared object triangles extends DrawMode() {}