shared abstract class DepthFunction()
        of off
        | never
        | less
        | equal
        | notEqual
        | lessOrEqual
        | greater
        | greaterOrEqual
        | always
{}

shared object off extends DepthFunction() {}
shared object never extends DepthFunction() {}
shared object less extends DepthFunction() {}
shared object equal extends DepthFunction() {}
shared object notEqual extends DepthFunction() {}
shared object lessOrEqual extends DepthFunction() {}
shared object greater extends DepthFunction() {}
shared object greaterOrEqual extends DepthFunction() {}
shared object always extends DepthFunction() {}