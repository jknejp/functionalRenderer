import option {
    Option,
    Some,
    none
}
import functionalRenderer.model.renderSettingValues {
    DepthFunction
}
shared class Subscene (
    shared Geometry geometry = Geometry(VertexBuffer(), IndexBuffer(), []),
    shared Option<ShaderProgram> shaderProgram = none,
    [Uniform<Matrix4>|Uniform<TextureUsage>|Uniform<RenderPass>|Uniform<Integer>|Uniform<Vector2<Integer>>
    |Uniform<Vector3<Integer>>|Uniform<Vector4<Integer>>|Uniform<Float>|Uniform<Vector2<Float>>|Uniform<Vector3<Float>>
    |Uniform<Vector4<Float>>*] uniformValues = [],
    shared [Subscene*] children = [],
    shared [Texture*] textures = [],
    shared Option<DepthFunction> depthFunction = none
) {
    shared Map<String, AnyUniform> uniforms = map([for (u in uniformValues) u.name-> u]);

    shared Option<AnyUniform> get(String name) {
        AnyUniform? u = uniforms.get(name);
        if(exists u) {
            return Some(u);
        } else {
            return none;
        }
    }
}