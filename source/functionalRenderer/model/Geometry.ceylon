import functionalRenderer.model.renderSettingValues {
    DrawMode,
    triangles
}
shared class Geometry(
        shared VertexBuffer vertexBuffer,
        shared IndexBuffer indexBuffer,
        shared [ShaderAttribute*] attributes,
        shared DrawMode drawMode = triangles) {
}