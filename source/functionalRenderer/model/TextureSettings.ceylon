import functionalRenderer.model.renderSettingValues {
    TextureFilterMipmapSupport,
    TextureFilter,
    TextureWrapFunction,
    nearestMipmapLinear,
    linear,
    repeat
}
shared class TextureSettings(
    shared TextureFilterMipmapSupport minFilter = nearestMipmapLinear,
    shared TextureFilter magFilter = linear,
    shared TextureWrapFunction wrapS = repeat,
    shared TextureWrapFunction wrapT = repeat
) {

}

shared TextureSettings defaultTextureSettings = TextureSettings();