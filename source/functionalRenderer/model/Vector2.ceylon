shared final class Vector2<Type>(shared Type x, shared Type y)
    satisfies Summable<Vector2<Type>> & Invertible<Vector2<Type>>
    given Type of Integer|Float satisfies Numeric<Type> {

    shared actual Vector2<Type> negated => Vector2(-x, -y);

    shared actual Vector2<Type> plus(Vector2<Type> that) => Vector2(this.x+that.x, this.y+that.y);
}