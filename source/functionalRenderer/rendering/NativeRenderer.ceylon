import functionalRenderer.model {
    ShaderProgram,
    FramebufferSetting,
    Colour,
    AnyUniform,
    GpuProgram,
    ShaderAttribute,
    GpuVertexBuffer,
    GpuIndexBuffer,
    GpuTexture,
    TextureImage,
    GpuFramebuffer,
    Viewport,
    TextureSettings,
    RenderResult
}
import functionalRenderer.model.renderSettingValues {
    DepthFunction,
    DrawMode
}
shared interface NativeRenderer {
    shared formal void refresh();
    shared formal void setDepthBuffer(DepthFunction depthFunction);
    shared formal void clear(Colour c);
    shared formal GpuVertexBuffer loadVertexBuffer([Float*] vertices);
    shared formal void setVertexBuffer(GpuVertexBuffer buffer);
    shared formal GpuIndexBuffer loadIndexBuffer([Integer*] indices);
    shared formal void setIndexBuffer(GpuIndexBuffer buffer);
    shared formal GpuProgram compileProgram(ShaderProgram shaderProgram);
    shared formal void useProgram(GpuProgram program);
    shared formal void setAttributes([ShaderAttribute*] attributes, GpuProgram program);
    shared formal void enableAttributes([ShaderAttribute*] attributes, GpuProgram program);
    shared formal void draw(DrawMode drawMode, Integer numberOfIndices);
    shared formal GpuTexture loadTexture(TextureImage image);
    shared formal void bindTexture(Integer slot, GpuTexture? texture, TextureSettings settings);
    shared formal void releaseTextures([Integer*] ids);
    shared formal void setFrameBuffer(GpuFramebuffer? framebuffer);
    shared formal GpuFramebuffer createFrameBuffer(FramebufferSetting setting);
    shared formal void bindUniform(AnyUniform uniform, GpuProgram program);
    shared formal void viewport(Viewport viewport);
    shared formal RenderResult readImage();
}