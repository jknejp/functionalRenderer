import functionalRenderer.model {
    SceneRoot,
    Subscene,
    Uniform,
    Matrix4,
    ShaderProgram,
    Texture,
    AnyUniform,
    GpuProgram,
    TextureImage,
    RenderPass,
    TextureUsage,
    Vector2,
    Vector3,
    Vector4,
    RenderResult
}
import functionalRenderer.model.renderSettingValues {
    DepthFunction
}
import option {
    Option,
    Some,
    toOption,
    none
}
import ceylon.time {
    now
}
import functionalRenderer.resourceManagement {
    ShaderProgramManager,
    VertexBufferManager,
    IndexBufferManager,
    TextureManager
}

shared class FunctionalRenderer(NativeRenderer renderer) {
    variable RenderingContext context = RenderingContext(renderer);

    shared RenderResult renderAndReadResult(SceneRoot root) {
        render(root);
        return renderer.readImage();
    }

    shared void render(SceneRoot root) {
        renderer.refresh();
        value renderPasses = findSceneRenderPasses(root);

        RenderingContext newContext = renderPasses.fold(context)((RenderingContext context, RenderPass renderPass) {
            value m = context.renderPassManager.setFrameBuffer(renderPass);
            return renderScene(renderPass.scene, context.copy{renderPassManager = m;});
        });

        renderer.setFrameBuffer(null);
        value c = renderScene(root, newContext);
        context = c.copy{renderPassManager = c.renderPassManager.reset();};
    }

    Iterable<RenderPass, Null> findSceneRenderPasses(SceneRoot root) {
        value scenePasses = findRenderPasses(root.children);
        value innerPasses = scenePasses.flatMap((RenderPass rp) => findSceneRenderPasses(rp.scene));

        return innerPasses.chain(scenePasses);
    }

    Iterable<RenderPass, Null> findRenderPasses([Subscene*] subscenes) {
        return subscenes.flatMap<RenderPass, Null>((Subscene subscene) {
            Boolean(String->AnyUniform) isRenderPass = (String key -> AnyUniform u) => u is Uniform<RenderPass>;
            {RenderPass*} passes = subscene.uniforms.filter(isRenderPass).map((String key -> AnyUniform u) {
                if(is Uniform<RenderPass> u) {
                    return u.val();
                } else {
                    throw Exception("Should not happen");
                }
            });

            return passes.chain(findRenderPasses(subscene.children));
        });
    }

    RenderingContext renderScene(SceneRoot root, RenderingContext context) {
        renderer.viewport(root.viewport);
        renderer.clear(root.clearColour);
        RenderingContext newContext = context.copy { programManager = context.programManager.selectProgram(root.shaderProgram); };
        return renderChildren(root.children, newContext, emptyMap, RenderSettings(root.depthFunction));
    }

    shared Texture getTexture(TextureImage image) {
        value texture = context.textureManager.loadTexture(image);
        return texture;
    }

    RenderingContext renderChildren([Subscene*] children, RenderingContext context, Map<String,AnyUniform> parentUniforms, RenderSettings parentSettings) {
        if (exists first = children.first) {
            RenderingContext newContext = renderSubscene(first, context, parentUniforms, parentSettings);
            return renderChildren(children.rest, newContext, parentUniforms, parentSettings);
        } else {
            return context;
        }
    }

    RenderingContext renderSubscene(Subscene subscene, RenderingContext context, Map<String,AnyUniform> parentUniforms, RenderSettings parentSettings) {
        Map<String,AnyUniform> mergedUniforms = mergeUniforms(subscene.uniforms, parentUniforms);
        RenderSettings mergedSettings = mergeSettings(subscene, parentSettings);

        RenderingContext newContext;
        if (subscene.geometry.vertexBuffer.vertices.size>0 && subscene.geometry.indexBuffer.indices.size>0) {
            newContext = drawSubscene(subscene, context, mergedUniforms, mergedSettings);
        } else {
            newContext = context;
        }

        return renderChildren(subscene.children, newContext, mergedUniforms, mergedSettings);
    }

    RenderingContext drawSubscene(Subscene subscene, RenderingContext context, Map<String,AnyUniform> mergedUniforms, RenderSettings mergedSettings) {
        renderer.setDepthBuffer(mergedSettings.deptFunction);

        IndexBufferManager newIndexManager = context.indexManager.selectIndexBuffer(subscene.geometry.indexBuffer);
        VertexBufferManager newVertexManager = context.vertexManager.selectVertexBuffer(subscene.geometry.vertexBuffer);
        ShaderProgramManager newProgramManager = subscene.shaderProgram.map(
                    (ShaderProgram a) => context.programManager.selectProgram(a)
        ).getOrDefault(context.programManager).useCurrentProgram(subscene.geometry.attributes);

        GpuProgram program = newProgramManager.currentOpenglProgram;
        TextureManager newTextureManager = mergedUniforms.fold(context.textureManager)((TextureManager m, String k -> AnyUniform u) {
            if(is Uniform<TextureUsage> u) {
                TextureUsage tex = u.val();
                value [newManager, slot] = m.bindTexture(tex.texture, tex.settings);
                renderer.bindUniform(Uniform(u.name, slot), program);
                return newManager;
            } else if(is Uniform<RenderPass> u) {
                RenderPass rp = u.val();
                value [newManager, slot] = m.bindGpuTexture(context.renderPassManager.getGpuTexture(rp), rp.textureSettings);
                renderer.bindUniform(Uniform(u.name, slot), program);
                return newManager;
            } else {
                return m;
            }
        });
        for (u in mergedUniforms) {
            value item = u.item;
            if(!is Uniform<TextureUsage> item) {
                renderer.bindUniform(item, program);
            }
        }
        renderer.draw(subscene.geometry.drawMode, subscene.geometry.indexBuffer.indices.size);

        RenderingContext newContext = context.copy {
            programManager = newProgramManager;
            vertexManager = newVertexManager;
            indexManager = newIndexManager;
            textureManager = newTextureManager.reset();
        };
        return newContext;
    }

    Map<String,AnyUniform> mergeUniforms(Map<String,AnyUniform> child, Map<String,AnyUniform> parent) {
        {String*} mergedKeys = child.keys.chain(parent.keys).distinct;

        return map([for (k in mergedKeys) k->calculateUniform(toOption(child.get(k)), toOption(parent.get(k)))]);
    }

    AnyUniform calculateUniform(Option<AnyUniform> child, Option<AnyUniform> parent) {
        Option<AnyUniform> merged = child.map((AnyUniform ch) =>
            switch (ch)
            case (is Uniform<Matrix4>)
                Uniform(ch.name, ch.val(parent.as<Uniform<Matrix4>>().map((Uniform<Matrix4> a) => a.val())))
            case (is Uniform<TextureUsage>)
                Uniform(ch.name, ch.val(parent.as<Uniform<TextureUsage>>().map((Uniform<TextureUsage> a) => a.val())))
            case (is Uniform<RenderPass>)
                Uniform(ch.name, ch.val(parent.as<Uniform<RenderPass>>().map((Uniform<RenderPass> a) => a.val())))
            case (is Uniform<Integer>)
                Uniform(ch.name, ch.val(parent.as<Uniform<Integer>>().map((Uniform<Integer> a) => a.val())))
            case (is Uniform<Vector2<Integer>>)
                Uniform(ch.name, ch.val(parent.as<Uniform<Vector2<Integer>>>().map((Uniform<Vector2<Integer>> a) => a.val())))
            case (is Uniform<Vector3<Integer>>)
                Uniform(ch.name, ch.val(parent.as<Uniform<Vector3<Integer>>>().map((Uniform<Vector3<Integer>> a) => a.val())))
            case (is Uniform<Vector4<Integer>>)
                Uniform(ch.name, ch.val(parent.as<Uniform<Vector4<Integer>>>().map((Uniform<Vector4<Integer>> a) => a.val())))
            case (is Uniform<Float>)
                Uniform(ch.name, ch.val(parent.as<Uniform<Float>>().map((Uniform<Float> a) => a.val())))
            case (is Uniform<Vector2<Float>>)
                Uniform(ch.name, ch.val(parent.as<Uniform<Vector2<Float>>>().map((Uniform<Vector2<Float>> a) => a.val())))
            case (is Uniform<Vector3<Float>>)
                Uniform(ch.name, ch.val(parent.as<Uniform<Vector3<Float>>>().map((Uniform<Vector3<Float>> a) => a.val())))
            case (is Uniform<Vector4<Float>>)
                Uniform(ch.name, ch.val(parent.as<Uniform<Vector4<Float>>>().map((Uniform<Vector4<Float>> a) => a.val())))
        );

        return merged.orElse(parent).getOrThrow("At least one of child or parent must exist");
    }

    RenderSettings mergeSettings(Subscene subscene, RenderSettings settings) {
        return subscene.depthFunction.map((DepthFunction func) {
            if(func != settings.deptFunction) {
                return RenderSettings(func);
            } else {
                return settings;
            }
        }).getOrDefault(settings);
    }

    class RenderSettings(shared DepthFunction deptFunction) {}
}