import functionalRenderer.resourceManagement {
    ShaderProgramManager,
    VertexBufferManager,
    IndexBufferManager,
    TextureManager,
    RenderPassManager
}
class RenderingContext {
    NativeRenderer renderer;
    shared ShaderProgramManager programManager;
    shared VertexBufferManager vertexManager;
    shared IndexBufferManager indexManager;
    shared TextureManager textureManager;
    shared RenderPassManager renderPassManager;
    shared new (NativeRenderer renderer, ShaderProgramManager programManager = ShaderProgramManager(renderer), VertexBufferManager vertexManager = VertexBufferManager(renderer), IndexBufferManager indexManager = IndexBufferManager(renderer), TextureManager textureManager = TextureManager(renderer), RenderPassManager renderPassManager = RenderPassManager(renderer)) {
        this.renderer = renderer;
        this.programManager = programManager;
        this.vertexManager = vertexManager;
        this.indexManager = indexManager;
        this.textureManager = textureManager;
        this.renderPassManager = renderPassManager;
    }


    shared RenderingContext copy(
            ShaderProgramManager programManager = this.programManager,
            VertexBufferManager vertexManager = this.vertexManager,
            IndexBufferManager indexManager = this.indexManager,
            TextureManager textureManager = this.textureManager,
            RenderPassManager renderPassManager = this.renderPassManager
            )
        => RenderingContext(renderer, programManager, vertexManager, indexManager, textureManager, renderPassManager);
}