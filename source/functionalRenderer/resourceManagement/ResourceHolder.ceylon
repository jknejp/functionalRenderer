import option {
    Option,
    Some,
    none,
    toOption
}
import java.util {
    JMap = Map,
    WeakHashMap
}
shared native class ResourceHolder<Element, ExtraInfo> given Element satisfies Object {
    shared native new (){}
    shared native void addResource(Element e, ExtraInfo info);
    shared native Option<ExtraInfo> getInfo(Element element);
}

shared native("jvm") class ResourceHolder<Element, ExtraInfo> given Element satisfies Object {
    JMap<Element, ExtraInfo> elements;
    shared native("jvm") new () {
        elements = WeakHashMap<Element, ExtraInfo>();
    }

    shared native("jvm") void addResource(Element e, ExtraInfo info) {
       elements.put(e, info);
    }

    shared native("jvm") Option<ExtraInfo> getInfo(Element element) {
        return toOption(elements.get(element));
    }
}

shared native("js") class ResourceHolder<Element, ExtraInfo> given Element satisfies Object {
    dynamic elements;
    shared native("js") new () {
        dynamic {
            elements = WeakMap();
        }
    }
    shared native("js") void addResource(Element e, ExtraInfo info) {
        dynamic {
            elements.set(e, info);
        }
    }
    shared native("js") Option<ExtraInfo> getInfo(Element element) {
        dynamic {
            dynamic elem = elements.get(element);
            if(exists elem) {
                return Some<ExtraInfo>(elem);
            } else {
                return none;
            }
        }
    }
}