import functionalRenderer.model {
    GpuVertexBuffer,
    VertexBuffer
}
import option {
    Option,
    none,
    Some
}
import functionalRenderer.rendering {
    NativeRenderer
}

shared class VertexBufferManager(
        NativeRenderer renderer,
        ResourceHolder<VertexBuffer,GpuVertexBuffer> buffers = ResourceHolder<VertexBuffer,GpuVertexBuffer>(),
        Option<VertexBuffer> currentBuffer = none) {

    shared VertexBufferManager selectVertexBuffer(VertexBuffer buffer) {
        if(buffers.getInfo(buffer).hasValue) {
            renderer.setVertexBuffer(buffers.getInfo(buffer).getOrThrow("Cannot find respective buffer"));
        } else {
            GpuVertexBuffer oglBuffer = renderer.loadVertexBuffer(buffer.vertices);
            buffers.addResource(buffer, oglBuffer);
        }
        return VertexBufferManager(renderer, buffers, Some(buffer));
    }
}