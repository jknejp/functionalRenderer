import functionalRenderer.model {
    RenderPass,
    GpuTexture,
    FramebufferSetting,
    GpuFramebuffer
}
import option {
    Option,
    Some,
    none
}
import functionalRenderer.rendering {
    NativeRenderer
}

shared class RenderPassManager(NativeRenderer renderer,
        Map<RenderPass,GpuFramebuffer> renderPassMap = emptyMap,
        [GpuFramebuffer*] freeFramebuffers = []) {

    shared RenderPassManager setFrameBuffer(RenderPass pass) {
        value setting = pass.framebufferSettings;
        Integer framebufferIndex = findFittingFramebuffer(setting);
        GpuFramebuffer? framebuffer = freeFramebuffers[framebufferIndex];
        if(exists framebuffer) {
            renderer.setFrameBuffer(framebuffer);
            value newFreeFramebuffers = freeFramebuffers[...framebufferIndex-1].chain(freeFramebuffers[framebufferIndex+1...]);
            return RenderPassManager(renderer, map(renderPassMap.follow(pass -> framebuffer)), newFreeFramebuffers.sequence());
        } else {
            GpuFramebuffer newFramebuffer = renderer.createFrameBuffer(setting);
            return RenderPassManager(renderer, map(renderPassMap.follow(pass -> newFramebuffer)), freeFramebuffers);
        }
    }

    shared GpuTexture getGpuTexture(RenderPass pass) {
        value framebuffer =  renderPassMap.get(pass);
        if(exists framebuffer) {
            return framebuffer.texture;
        } else {
            throw Exception("Render pass haven’t been rendered");
        }
    }

    shared RenderPassManager reset() => RenderPassManager(renderer, emptyMap, freeFramebuffers.chain(renderPassMap.items).sequence());

    Integer findFittingFramebuffer(FramebufferSetting settings) {
        for(f in freeFramebuffers.indexed) {
            if(f.item.settings == settings) {
                return f.key;
            }
        }

        return -1;
    }
}