import functionalRenderer.model {
    IndexBuffer,
    GpuIndexBuffer
}
import option {
    Option,
    none,
    Some
}
import functionalRenderer.rendering {
    NativeRenderer
}
shared class IndexBufferManager(
        NativeRenderer renderer,
        ResourceHolder<IndexBuffer, GpuIndexBuffer> buffers = ResourceHolder<IndexBuffer, GpuIndexBuffer>(),
        Option<IndexBuffer> currentBuffer = none) {

    shared IndexBufferManager selectIndexBuffer(IndexBuffer buffer) {
        if(buffers.getInfo(buffer).hasValue) {
            renderer.setIndexBuffer(buffers.getInfo(buffer).getOrThrow());
            return IndexBufferManager(renderer, buffers, Some(buffer));
        } else {
            GpuIndexBuffer oglBuffer = renderer.loadIndexBuffer(buffer.indices);
            buffers.addResource(buffer, oglBuffer);
            return IndexBufferManager(renderer, buffers, Some(buffer));
        }
    }
}