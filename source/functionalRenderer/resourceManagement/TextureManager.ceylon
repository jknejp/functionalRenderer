import option {
    Option,
    none
}
import functionalRenderer.model {
    Texture,
    GpuTexture,
    TextureImage,
    TextureSettings,
    defaultTextureSettings
}
import functionalRenderer.rendering {
    NativeRenderer
}

shared class TextureManager(
        NativeRenderer renderer,
        ResourceHolder<Texture, GpuTexture> textures = ResourceHolder<Texture, GpuTexture>(),
        Integer activeTexturesCount = 0) {

    shared Texture loadTexture(TextureImage image) {
        GpuTexture gpuTex = renderer.loadTexture(image);
        Texture texture = Texture();
        textures.addResource(texture, gpuTex);
        return texture;
    }

    shared [TextureManager, Integer] bindTexture(Texture texture, TextureSettings settings) {
        return bindGpuTexture(textures.getInfo(texture).getOrThrow(), settings);
    }

    shared [TextureManager, Integer] bindGpuTexture(GpuTexture texture, TextureSettings settings) {
        renderer.bindTexture(activeTexturesCount, texture, settings);
        return [TextureManager(renderer, textures, activeTexturesCount + 1), activeTexturesCount];
    }

    shared TextureManager reset() {
        if(activeTexturesCount == 0) {
            return this;
        } else {
            for (slot in 0..activeTexturesCount - 1) {
                renderer.bindTexture(activeTexturesCount, null, defaultTextureSettings);
            }
            return TextureManager(renderer, textures, 0);
        }
    }
}