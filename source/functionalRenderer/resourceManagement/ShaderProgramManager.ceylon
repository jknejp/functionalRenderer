import functionalRenderer.model {
    ShaderProgram,
    GpuProgram,
    ShaderAttribute
}
import option {
    none,
    Option,
    Some
}
import functionalRenderer.rendering {
    NativeRenderer
}
shared class ShaderProgramManager(
        NativeRenderer renderer,
        ResourceHolder<ShaderProgram, GpuProgram> programs = ResourceHolder<ShaderProgram,GpuProgram>(),
        ResourceHolder<ShaderProgram, [ShaderAttribute*]> programAttributes = ResourceHolder<ShaderProgram, [ShaderAttribute*]>(),
        Option<ShaderProgram> currentProgram = none) {

    shared ShaderProgramManager selectProgram(ShaderProgram program) {
        Boolean sameProgram = currentProgram.filter((ShaderProgram sp) => sp == program).hasValue;
        value existingProgram = programs.getInfo(program);
        if(sameProgram) {
            return this;
        } else if (existingProgram.hasValue) {
            return ShaderProgramManager(renderer, programs, programAttributes, Some(program));
        } else {
            GpuProgram oglProgram = renderer.compileProgram(program);
            programs.addResource(program, oglProgram);

            return ShaderProgramManager(renderer, programs, programAttributes, Some(program));
        }
    }

    shared ShaderProgramManager useCurrentProgram([ShaderAttribute*] attributes) {
        value program = currentProgram.getOrThrow("No program set.");
        value gpuProgram = programs.getInfo(program).getOrThrow();
        if(!programAttributes.getInfo(program).filter((ShaderAttribute[] a) => a == attributes).hasValue) {
            renderer.setAttributes(attributes, programs.getInfo(program).getOrThrow());
            programAttributes.addResource(program, attributes);
        }
        renderer.useProgram(gpuProgram);
        renderer.enableAttributes(attributes, gpuProgram);
        return this;
    }

    shared GpuProgram currentOpenglProgram {
        return programs.getInfo(currentProgram.getOrThrow("Program not set")).getOrThrow("Program not found");
    }
}