shared interface Option<out Type> of Some<Type> | none {
    shared formal Boolean hasValue;
    shared formal Type|DefaultType getOrDefault<DefaultType>(DefaultType default);
    shared formal Type getOrThrow(String message = "Optional value doesn’t exist");
    shared formal Option<ReturnType> map<ReturnType>(ReturnType(Type) func);
    shared formal Option<ReturnType> flatMap<ReturnType>(Option<ReturnType>(Type) func);
    shared formal Option<Type|DefaultType> orElse<DefaultType>(Option<DefaultType> alternative);
    shared formal Option<Type> filter(Boolean(Type) condition);
    shared formal Option<FilterType> as<FilterType>();
}

shared class Some<out Type>(Type val) satisfies Option<Type> {
    shared actual Boolean hasValue => true;
    shared actual Type getOrDefault<DefaultType>(DefaultType default) => val;
    shared actual Type getOrThrow(String message) => val;
    shared actual Option<ReturnType> map<ReturnType>(ReturnType(Type) func) => Some(func(val));
    shared actual Option<ReturnType> flatMap<ReturnType>(Option<ReturnType>(Type) func) => func(val);
    shared actual Option<Type|DefaultType> orElse<DefaultType>(Option<DefaultType> alternative) => this;
    shared actual Option<Type> filter(Boolean(Type) condition) => if(condition(val)) then this else none;
    shared actual Option<FilterType> as<FilterType>() {
        if (is FilterType val) {
            return Some(val);
        } else {
            return none;
        }
    }
}

shared object none satisfies Option<Nothing> {
    shared actual Boolean hasValue => false;
    shared actual DefaultType getOrDefault<DefaultType>(DefaultType default) => default;
    shared actual Nothing getOrThrow(String message) {throw Exception(message);}
    shared actual Option<ReturnType> map<ReturnType>(ReturnType(Nothing) func) => this;
    shared actual Option<ReturnType> flatMap<ReturnType>(Option<ReturnType>(Nothing) func) => this;
    shared actual Option<DefaultType> orElse<DefaultType>(Option<DefaultType> alternative) => alternative;
    shared actual Option<Nothing> filter(Boolean(Nothing) condition) => this;
    shared actual Option<FilterType> as<FilterType>() => this;
}

shared Option<Type> toOption<Type>(Type? val) {
    if(exists val) {
        return Some(val);
    } else {
        return none;
    }
}