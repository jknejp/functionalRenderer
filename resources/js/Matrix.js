var Matrix4 = function (data) {
    this.data = data;
};

var p = Matrix4.prototype;

p.getData = function () {
    return this.data;
};

p.translateX = function (distance) {
    var translation = [
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        distance, 0.0, 0.0, 1.0
    ];
    return this.mul(new Matrix4(translation));
};

p.translateY = function (distance) {
    var translation = [
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, distance, 0.0, 1.0
    ];
    return this.mul(new Matrix4(translation));
};

p.translateZ = function (distance) {
    var translation = [
        1.0, 0.0, 0.0, 0.0,
        0.0, 1.0, 0.0, 0.0,
        0.0, 0.0, 1.0, 0.0,
        0.0, 0.0, distance, 1.0
    ];
    return this.mul(new Matrix4(translation));
};

p.rotateX = function (angle) {
    var translation = [
        1.0, 0.0, 0.0, 0.0,
        0.0, Math.cos(angle), -Math.sin(angle), 0.0,
        0.0, Math.sin(angle), Math.cos(angle), 0.0,
        0.0, 0.0, 0.0, 1.0
    ];
    return this.mul(new Matrix4(translation));
};

p.rotateY = function (angle) {
    var translation = [
        Math.cos(angle),0.0, Math.sin(angle), 0.0,
        0.0, 1.0, 0.0, 0.0,
        -Math.sin(angle), 0.0, Math.cos(angle), 0.0,
        0.0, 0.0, 0.0, 1.0
    ];
    return this.mul(new Matrix4(translation));
};

p.scale = function (ratio) {
    var translation = [
        ratio, 0.0, 0.0, 0.0,
        0.0, ratio, 0.0, 0.0,
        0.0, 0.0, ratio, 0.0,
        0.0, 0.0, 0.0, 1.0
    ];
    return this.mul(new Matrix4(translation));
};

p.mul = function(other) {
    var data = this.data;

    var result = [
        data[0 * 4 + 0] * other.data[0 * 4 + 0] + data[0 * 4 + 1] * other.data[1 * 4 + 0] + data[0 * 4 + 2] * other.data[2 * 4 + 0] + data[0 * 4 + 3] * other.data[3 * 4 + 0],
        data[0 * 4 + 0] * other.data[0 * 4 + 1] + data[0 * 4 + 1] * other.data[1 * 4 + 1] + data[0 * 4 + 2] * other.data[2 * 4 + 1] + data[0 * 4 + 3] * other.data[3 * 4 + 1],
        data[0 * 4 + 0] * other.data[0 * 4 + 2] + data[0 * 4 + 1] * other.data[1 * 4 + 2] + data[0 * 4 + 2] * other.data[2 * 4 + 2] + data[0 * 4 + 3] * other.data[3 * 4 + 2],
        data[0 * 4 + 0] * other.data[0 * 4 + 3] + data[0 * 4 + 1] * other.data[1 * 4 + 3] + data[0 * 4 + 2] * other.data[2 * 4 + 3] + data[0 * 4 + 3] * other.data[3 * 4 + 3],
        data[1 * 4 + 0] * other.data[0 * 4 + 0] + data[1 * 4 + 1] * other.data[1 * 4 + 0] + data[1 * 4 + 2] * other.data[2 * 4 + 0] + data[1 * 4 + 3] * other.data[3 * 4 + 0],
        data[1 * 4 + 0] * other.data[0 * 4 + 1] + data[1 * 4 + 1] * other.data[1 * 4 + 1] + data[1 * 4 + 2] * other.data[2 * 4 + 1] + data[1 * 4 + 3] * other.data[3 * 4 + 1],
        data[1 * 4 + 0] * other.data[0 * 4 + 2] + data[1 * 4 + 1] * other.data[1 * 4 + 2] + data[1 * 4 + 2] * other.data[2 * 4 + 2] + data[1 * 4 + 3] * other.data[3 * 4 + 2],
        data[1 * 4 + 0] * other.data[0 * 4 + 3] + data[1 * 4 + 1] * other.data[1 * 4 + 3] + data[1 * 4 + 2] * other.data[2 * 4 + 3] + data[1 * 4 + 3] * other.data[3 * 4 + 3],
        data[2 * 4 + 0] * other.data[0 * 4 + 0] + data[2 * 4 + 1] * other.data[1 * 4 + 0] + data[2 * 4 + 2] * other.data[2 * 4 + 0] + data[2 * 4 + 3] * other.data[3 * 4 + 0],
        data[2 * 4 + 0] * other.data[0 * 4 + 1] + data[2 * 4 + 1] * other.data[1 * 4 + 1] + data[2 * 4 + 2] * other.data[2 * 4 + 1] + data[2 * 4 + 3] * other.data[3 * 4 + 1],
        data[2 * 4 + 0] * other.data[0 * 4 + 2] + data[2 * 4 + 1] * other.data[1 * 4 + 2] + data[2 * 4 + 2] * other.data[2 * 4 + 2] + data[2 * 4 + 3] * other.data[3 * 4 + 2],
        data[2 * 4 + 0] * other.data[0 * 4 + 3] + data[2 * 4 + 1] * other.data[1 * 4 + 3] + data[2 * 4 + 2] * other.data[2 * 4 + 3] + data[2 * 4 + 3] * other.data[3 * 4 + 3],
        data[3 * 4 + 0] * other.data[0 * 4 + 0] + data[3 * 4 + 1] * other.data[1 * 4 + 0] + data[3 * 4 + 2] * other.data[2 * 4 + 0] + data[3 * 4 + 3] * other.data[3 * 4 + 0],
        data[3 * 4 + 0] * other.data[0 * 4 + 1] + data[3 * 4 + 1] * other.data[1 * 4 + 1] + data[3 * 4 + 2] * other.data[2 * 4 + 1] + data[3 * 4 + 3] * other.data[3 * 4 + 1],
        data[3 * 4 + 0] * other.data[0 * 4 + 2] + data[3 * 4 + 1] * other.data[1 * 4 + 2] + data[3 * 4 + 2] * other.data[2 * 4 + 2] + data[3 * 4 + 3] * other.data[3 * 4 + 2],
        data[3 * 4 + 0] * other.data[0 * 4 + 3] + data[3 * 4 + 1] * other.data[1 * 4 + 3] + data[0 * 4 + 3] * other.data[2 * 4 + 3] + data[3 * 4 + 3] * other.data[3 * 4 + 3]
    ];

    return new Matrix4(result);
};

p.transpose = function () {
    var data = this.data;

    var transposedData = [
        data[0 * 4 + 0],
        data[1 * 4 + 0],
        data[2 * 4 + 0],
        data[3 * 4 + 0],
        data[0 * 4 + 1],
        data[1 * 4 + 1],
        data[2 * 4 + 1],
        data[3 * 4 + 1],
        data[0 * 4 + 2],
        data[1 * 4 + 2],
        data[2 * 4 + 2],
        data[3 * 4 + 2],
        data[0 * 4 + 3],
        data[1 * 4 + 3],
        data[2 * 4 + 3],
        data[3 * 4 + 3]
    ];

    return new Matrix4(transposedData);
};

p.inverse = function () {
    var data = this.data;

    var s0 = data[0 * 4 + 0] * data[1 * 4 + 1] - data[1 * 4 + 0] * data[0 * 4 + 1];
    var s1 = data[0 * 4 + 0] * data[1 * 4 + 2] - data[1 * 4 + 0] * data[0 * 4 + 2];
    var s2 = data[0 * 4 + 0] * data[1 * 4 + 3] - data[1 * 4 + 0] * data[0 * 4 + 3];
    var s3 = data[0 * 4 + 1] * data[1 * 4 + 2] - data[1 * 4 + 1] * data[0 * 4 + 2];
    var s4 = data[0 * 4 + 1] * data[1 * 4 + 3] - data[1 * 4 + 1] * data[0 * 4 + 3];
    var s5 = data[0 * 4 + 2] * data[1 * 4 + 3] - data[1 * 4 + 2] * data[0 * 4 + 3];
    var c5 = data[2 * 4 + 2] * data[3 * 4 + 3] - data[3 * 4 + 2] * data[2 * 4 + 3];
    var c4 = data[2 * 4 + 1] * data[3 * 4 + 3] - data[3 * 4 + 1] * data[2 * 4 + 3];
    var c3 = data[2 * 4 + 1] * data[3 * 4 + 2] - data[3 * 4 + 1] * data[2 * 4 + 2];
    var c2 = data[2 * 4 + 0] * data[3 * 4 + 3] - data[3 * 4 + 0] * data[2 * 4 + 3];
    var c1 = data[2 * 4 + 0] * data[3 * 4 + 2] - data[3 * 4 + 0] * data[2 * 4 + 2];
    var c0 = data[2 * 4 + 0] * data[3 * 4 + 1] - data[3 * 4 + 0] * data[2 * 4 + 1];

    var inverseDeterminant = 1 / (s0 * c5 - s1 * c4 + s2 * c3 + s3 * c2 - s4 * c1 + s5 * c0);

    var resultData = [
        (data[1 * 4 + 1] * c5 - data[1 * 4 + 2] * c4 + data[1 * 4 + 3] * c3) * inverseDeterminant,
        (-data[0 * 4 + 1] * c5 + data[0 * 4 + 2] * c4 - data[0 * 4 + 3] * c3) * inverseDeterminant,
        (data[3 * 4 + 1] * s5 - data[3 * 4 + 2] * s4 + data[3 * 4 + 3] * s3) * inverseDeterminant,
        (-data[2 * 4 + 1] * s5 + data[2 * 4 + 2] * s4 - data[2 * 4 + 3] * s3) * inverseDeterminant,
        (-data[1 * 4 + 0] * c5 + data[1 * 4 + 2] * c2 - data[1 * 4 + 3] * c1) * inverseDeterminant,
        (data[0 * 4 + 0] * c5 - data[0 * 4 + 2] * c2 + data[0 * 4 + 3] * c1) * inverseDeterminant,
        (-data[3 * 4 + 0] * s5 + data[3 * 4 + 2] * s2 - data[3 * 4 + 3] * s1) * inverseDeterminant,
        (data[2 * 4 + 0] * s5 - data[2 * 4 + 2] * s2 + data[2 * 4 + 3] * s1) * inverseDeterminant,
        (data[1 * 4 + 0] * c4 - data[1 * 4 + 1] * c2 + data[1 * 4 + 3] * c0) * inverseDeterminant,
        (-data[0 * 4 + 0] * c4 + data[0 * 4 + 1] * c2 - data[0 * 4 + 3] * c0) * inverseDeterminant,
        (data[3 * 4 + 0] * s4 - data[3 * 4 + 1] * s2 + data[3 * 4 + 3] * s0) * inverseDeterminant,
        (-data[2 * 4 + 0] * s4 + data[2 * 4 + 1] * s2 - data[2 * 4 + 3] * s0) * inverseDeterminant,
        (-data[1 * 4 + 0] * c3 + data[1 * 4 + 1] * c1 - data[1 * 4 + 2] * c0) * inverseDeterminant,
        (data[0 * 4 + 0] * c3 - data[0 * 4 + 1] * c1 + data[0 * 4 + 2] * c0) * inverseDeterminant,
        (-data[3 * 4 + 0] * s3 + data[3 * 4 + 1] * s1 - data[3 * 4 + 2] * s0) * inverseDeterminant,
        (data[2 * 4 + 0] * s3 - data[2 * 4 + 1] * s1 + data[2 * 4 + 2] * s0) * inverseDeterminant
    ];

    return new Matrix4(resultData);
};

Matrix4.identityData = [
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, 1.0, 0.0,
    0.0, 0.0, 0.0, 1.0
];

Matrix4.identity = new Matrix4(Matrix4.identityData);