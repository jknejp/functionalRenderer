var firstRender = true;

var indexBuffer;
var vertexBuffer;

var teapotIndexBufferId;
var teapotVertexBufferId;
var teapotIndices;
var bunnyIndexBufferId;
var bunnyVertexBufferId;
var bunnyindices;

var programId;

var projection = new Matrix4([
    1.74, 0.0, 0.0, 0.0,
    0.0, 1.74, 0.0, 0.0,
    0.0, 0.0, -1.0, -1.0,
    0.0, 0.0, -0.2, 0.0
]);

var view = new Matrix4([
    1.0, 0.0, 0.0, 0.0,
    0.0, 1.0, -0.0, 0.0,
    -0.0, 0.0, 1.0, 0.0,
    -0.0, -0.0, -4.0, 1.0
]);

window.javascriptRenderer = {
    render: function (gl, time, changeGeometry) {
        if(firstRender) {
            var vertexShader = readShaderProgram("light_vert");
            var fragmentShader = readShaderProgram("light_frag");
            programId = createShader(gl, vertexShader, fragmentShader);
            loadTeapot();
            teapotIndexBufferId = loadIndexBuffer(gl, indexBuffer);
            teapotVertexBufferId = loadVertexBuffer(gl, vertexBuffer);
            teapotIndices = indexBuffer.length;
            loadBunny();
            bunnyIndexBufferId = loadIndexBuffer(gl, indexBuffer);
            bunnyVertexBufferId = loadVertexBuffer(gl, vertexBuffer);
            bunnyIndices = indexBuffer.length;
            gl.bindAttribLocation(programId, 0, "aVertexPosition");
            gl.bindAttribLocation(programId, 1, "aNormal");
            gl.linkProgram(programId);
            firstRender = false;
        }
        gl.viewport(0, 0, 640, 640);
        gl.clearColor(0, 0, 0, 1);
        gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);
        if(changeGeometry && time % 2 == 0) {
            gl.bindBuffer(gl.ARRAY_BUFFER, bunnyVertexBufferId);
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, bunnyIndexBufferId);
        } else {
            gl.bindBuffer(gl.ARRAY_BUFFER, teapotVertexBufferId);
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, teapotIndexBufferId);
        }
        gl.enable(gl.DEPTH_TEST);
        gl.depthFunc(gl.LEQUAL);
        gl.useProgram(programId);

        gl.enableVertexAttribArray(0);
        gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 6 * 4, 0);
        gl.enableVertexAttribArray(1);
        gl.vertexAttribPointer(1, 3, gl.FLOAT, false, 6 * 4, 3 * 4);

        var indicesNumber = (changeGeometry && time % 2 == 0) ? bunnyIndices : teapotIndices;
        var model = Matrix4.identity.rotateY(time/1000.0);
        drawObject(gl, model, indicesNumber);

        model = Matrix4.identity.translateY(-0.5).scale(0.3).rotateY(-time/300.0).translateY(0.5).translateZ(1.0).rotateY(time/1000.0);
        drawObject(gl, model, indicesNumber);

        model = Matrix4.identity.translateY(-0.5).scale(0.3).rotateY(-time/300.0).translateY(0.5).translateZ(1.0).rotateY(time/1000.0).rotateY(Math.PI/2);
        drawObject(gl, model, indicesNumber);

        model = Matrix4.identity.translateY(-0.5).scale(0.3).rotateY(-time/300.0).translateY(0.5).translateZ(1.0).rotateY(time/1000.0).rotateY(Math.PI);
        drawObject(gl, model, indicesNumber);

        model = Matrix4.identity.translateY(-0.5).scale(0.3).rotateY(-time/300.0).translateY(0.5).translateZ(1.0).rotateY(time/1000.0).rotateY(-Math.PI/2);
        drawObject(gl, model, indicesNumber);
    }
};

function drawObject(gl, model, indicesCount) {
    setUniformMatrix(gl, programId, "view", view);
    setUniformMatrix(gl, programId, "model", model);
    setUniformMatrix(gl, programId, "proj", projection);
    setUniformMatrix(gl, programId, "normal", model.inverse().transpose());

    gl.drawElements(gl.TRIANGLES, indicesCount, gl.UNSIGNED_SHORT, 0);
}

function readShaderProgram(name) {
    var shader = window.shaders[name];
    shader = shader.replace(/#version[^\n]*/, "");

    return shader;
}

function createShader(gl, vertexSrc, fragmentSrc) {
    var vShader = gl.createShader(gl.VERTEX_SHADER);
    gl.shaderSource(vShader, vertexSrc);
    gl.compileShader(vShader);

    var fShader = gl.createShader(gl.FRAGMENT_SHADER);
    gl.shaderSource(fShader, fragmentSrc);
    gl.compileShader(fShader);

    var programId = gl.createProgram();
    gl.attachShader(programId, vShader);
    gl.attachShader(programId, fShader);

    return programId;
}

function loadIndexBuffer(gl, indices) {
    var buffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);
    return buffer;
}

function loadVertexBuffer(gl, vertices) {
    var buffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, buffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
    return buffer;
}

function setUniformMatrix(gl, program, name, value) {
    var location = gl.getUniformLocation(program, name);
    gl.uniformMatrix4fv(location, false, new Float32Array(value.transpose().getData()));
}

function loadTeapot() {
    loadObj("wt_teapot");
}

function loadBunny() {
    loadObj("bunny");
}


function loadObj(name) {
    var vertices = [];
    var normals = [];
    var indices = [];
    var normalIndices = [];

    var lines = window.objects[name].split("\n");
    for(var i = 0; i < lines.length; ++i) {
        var line = lines[i];
        var split = line.split(" ");
        switch (split[0]) {
            case "v":
                for(var j = 1; j < 4; ++j) {
                    vertices.push(parseFloat(split[j]));
                }
                break;
            case "f":
                for(var k = 1; k < 4; ++k) {
                    var innerSplit = split[k].split("/");
                    var first = parseInt(innerSplit[0]) - 1;
                    var last = parseInt(innerSplit[2]) - 1;
                    indices.push(first);
                    normalIndices.push(last);
                }
                break;
            case "vn":
                for(var l = 1; l < 4; ++l) {
                    normals.push(parseFloat(split[l]));
                }
                break;
        }
    }

    var size = indices.length;
    vertexBuffer = [];
    indexBuffer = [];
    for(var m = 0; m < size; ++m) {
        var ind = indices[m] * 3;
        var normInd = normalIndices[m] * 3;
        vertexBuffer[m * 6 + 0] = vertices[ind + 0];
        vertexBuffer[m * 6 + 1] = vertices[ind + 1];
        vertexBuffer[m * 6 + 2] = vertices[ind + 2];
        vertexBuffer[m * 6 + 3] = normals[normInd + 0];
        vertexBuffer[m * 6 + 4] = normals[normInd + 1];
        vertexBuffer[m * 6 + 5] = normals[normInd + 2];
        indexBuffer[m] = m;
    }
}
