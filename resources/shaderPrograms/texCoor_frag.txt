#version 330

precision mediump float;

varying vec2 texCoor;
uniform sampler2D uTex;

void main() {
    gl_FragColor = texture2D(uTex, texCoor);
}